<?php
/**
 * Task List Plugin Library
 *
 * Library of functions for the Task List
 * @author Ramona
 * @package Task-list
 * @version 1.0
 */

/**
 * Library class
 * @package Task list
 * @subpackage includes
 */
 
class RFP_Lib {
	
		/**
	 * Create database table and add default options
	 * @static
	 */
	 
function install_plugin () {
	global $wpdb;

   $table_name	=	$wpdb->prefix . "rfpscreenoptions";
   $table_cat	=	$wpdb->prefix . "rfp_catuser";

if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {

$sql = "CREATE TABLE $table_name (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `priority` VARCHAR( 55 ) NOT NULL ,
  `duedate` VARCHAR( 10 ) NOT NULL ,
  `assigned` VARCHAR( 10 ) NOT NULL ,
  UNIQUE KEY id (id))";


require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql_ins	=	$wpdb->query("INSERT INTO wp_rfpscreenoptions(priority,duedate,assigned)VALUES('1','1','1')");


			}
			
if($wpdb->get_var("SHOW TABLES LIKE '$table_cat'") != $table_cat) {

$sql = "CREATE TABLE $table_cat (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `cat_id` VARCHAR( 10 ) NOT NULL ,
  `username` VARCHAR( 55 ) NOT NULL ,
    UNIQUE KEY id (id))";
	
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

		}
			
	}
function register_my_taxonomies() {

	register_taxonomy(
		'rfpcat',
		array( 'post' ),
		array(
		'public' => true,
		'labels' => array(
		'name' => __( 'rfpcat' ),
		'singular_name' => __( 'rfpcat' )
			),
		)
	);
}
	
function populate_taxonomies() {
			  global $wpdb;
              wp_insert_term('Website Design','rfpcat');
			  $id1			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id1."','1')");
              wp_insert_term('SEO','rfpcat');
			  $id2			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id2."','1')");
			  wp_insert_term('PPC','rfpcat');
			  $id3			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id3."','1')");
			  wp_insert_term('PR','rfpcat');
			  $id4			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id4."','1')");
			  wp_insert_term('Social Media','rfpcat');
			  $id5			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id5."','1')");
			  wp_insert_term('Email','rfpcat');
			  $id6			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id6."','1')");
			  wp_insert_term('Affiliate','rfpcat');
			  $id7			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id7."','1')");
			  wp_insert_term('Analytics','rfpcat');
			  $id8			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id8."','1')");
			  wp_insert_term('Landing Pages','rfpcat');
			  $id9			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id9."','1')");
			  wp_insert_term('Content Creation','rfpcat');
			  $id10			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id10."','1')");
			  wp_insert_term('Blog','rfpcat');
			  $id11			=	$wpdb->insert_id;
			  $sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id11."','1')");
         }	
	
	/**
	 * Set Plugin Default Options

	 */
public static function set_options() {
	$general_options 	=	array('priority'=> 1,'category'=> 0,	'timeframe'=> 0, 'assignedto'=> 0, 'status'=> 0, 'notes'=> 0, 'urladdress'=> 0, 'date'=> 0, 'wysiwyg'=> 1);
	
	$advanced_options	=	array('date_format'=> 'm/d/Y','taskdisplay' => 10, 'priority_0'=>  __( 'High', 'rfp-task-list' ), 'priority_1'=> __( 'Medium', 'rfp-task-list'), 'priority_2'=> __( 'Low', 'rfp-task-list'), 'show_id'=> 0, 'show_date_added'=> 0,'ShowRecurring'=> 0,'RecurringTask'=> 1, 'assign'=> 1, 'show_only_assigned'=> 1, 'user_roles'=> array(0=>'administrator', 1=>'editor', 2=>'author', 3=>'contributor', 4=>'subscriber'), 'email_assigned'=>0, 'email_from'=> html_entity_decode( get_bloginfo( 'name' ) ), 'email_subject'=> __( 'A RFP-Task list item has been assigned to you', 'rfp-task-list' ), 'email_text'=> __( 'The following item has been assigned to you:', 'rfp-task-list' ), 'email_category'=> '1', 'email_show_assigned_by'=> '0');
	
	$permissions_options = array('view_capability'=> 'edit_posts', 'add_capability'=> 'edit_posts', 'edit_capability'=> 'edit_posts', 'delete_capability'=> 'manage_options', 'purge_capability'=> 'manage_options', 'complete_capability'=> 'edit_posts', 'assign_capability'=> 'manage_options',	'view_all_assigned_capability' => 'manage_options', 'add_cat_capability'=> 'manage_options');
			add_option( 'rfplist_general', $general_options );
			add_option( 'rfplist_advanced', $advanced_options );
			add_option( 'rfplist_permissions', $permissions_options);

	}
public static function general_settings($data) {
	//print_r($data);
		 update_option('rfplist_general',$data);
		 ?>
		 <script language="javascript1.5" type="text/javascript">
			 alert("Task List Settings has been updated successfully!");
			 window.location='<?php echo admin_url(); ?>admin.php?page=Settings';
		 </script>
		 <?php
		 
	}
public static function advanced_settings($data) {

	update_option('rfplist_advanced',$data);
	
	?>
	<script type="text/javascript" language="javascript1.5">
		alert("Advanced Settings has been updated successfully!");
		window.location='<?php echo admin_url(); ?>admin.php?page=Settings&tab=advanced';
	</script>
	<?php
}
public static function permissions_settongs($data) {

	update_option('rfplist_permissions', $data);
	
	?>
	<script type="text/javascript" language="javascript1.5">
		alert("User Permissions has been updated successfully!");
		window.location='<?php echo admin_url(); ?>admin.php?page=Settings&tab=userpermissions';
	</script>
	<?php
}
public static function screenoptions() {

global $wpdb;

$sql_update	=	$wpdb->query("UPDATE wp_rfpscreenoptions SET priority ='".$_REQUEST['Priority']."',duedate ='".$_REQUEST['Datedue']."',assigned='".$_REQUEST['Assignedto']."' WHERE id = 1");
?>
<script type="text/javascript" language="javascript1.5">
		alert("settings Options updated successfully!");
		window.location='<?php echo admin_url(); ?>admin.php?page=my-menu';
	</script>
<?php

}

}
?>