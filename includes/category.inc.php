<?php
/**
 * Task List Plugin Categories
 * @author Ramona
 * @package task-list
 * @version 1.0
 * @rfptask add meta value for sort order and enable
 */

class RFP_Category {

public static function get_categories() {
		$categories = get_terms( 'rfpcat', '&hide_empty=0' );
		return $categories;
		
	}
public static function get_categories_id() {	
	$categories2 = get_the_category( $post_id );
	return $categories2;
		
}

/**
	 * Add a new RFP_Task list category to the taxonomy
	  */
	public static function InsertCategory() {
		global $wpdb;
		$CategoyTerm = wp_insert_term( $_REQUEST['CategoryName'],'rfpcat');
		//print_r($CategoyTerm);
		$id			=	$wpdb->insert_id;
		$sql_ins	=	$wpdb->query("INSERT INTO wp_rfp_catuser(cat_id,username)VALUES('".$id."','".$_REQUEST['AssignedTo']."')");
		$user_id 	=	get_current_user_id();
		//  echo $user_id;
		if ( isset( $_REQUEST['AssignedTo'] ) )
		$created =  add_user_meta( $user_id, '_cateassignedto', $_REQUEST['AssignedTo'] );
		//print_r($created);
	//	exit;
		
			//add_user_meta( $CategoyTerm, '_cateassignedto', $cateassignedto, true );
	
}
	/**
	 * Update RFP-Task category
	 */
	public static function UpdateCategory() {
		$category_id = absint( $_POST['txt_id'] );
		//print_r($_REQUEST);
		$term = wp_update_term( $category_id, 'rfpcat', array( 'name' => $_POST['CategoryName'] ) );
		//print_r($term);die()
		?>
		<script type="text/javascript" language="javascript1.5">
			alert('Task Category has been updated successfully!');
			window.location='<?php echo admin_url(); ?>admin.php?page=Category-List';
			</script>
		<?php
		
		
	}
	/** 
	*Delete RFP-Task Category
	*/
	public static function DeleteCategory() {
	error_reporting(0);
	//print_r($_REQUEST);
	foreach($_REQUEST['checkbox'] as $delete_id) {
		wp_delete_term( absint( $delete_id ), 'rfpcat' );
	}
		
	}
		
		
	
 /**
	 * Create Taxonomies Categories
		 */
	public static function CreateTaxonomies() {
		$labels = array('name'=> _x( 'Categories', 'taxonomy general name', 'rfp-task-list' ),'singular_name' => _x( 'Category', 'taxonomy singular name', 'rfp-task-list' ),);

		register_taxonomy( 'rfpcat', array( 'RFP_Task' ), array('hierarchical' => true,'labels' => $labels,'show_ui' => false,'query_var'=> false,'rewrite'=> false,) );
	}


	
 /**
	 * Create the category management page
	 */
	public static function category_page() {
	//Get the category ID
	
	$RFP_Task_message = '';
		$RFP_Task_Action = '';
		//print_r($_REQUEST);
		if ( isset( $_REQUEST['RFP_Task_Action'] ) ) {
		 $RFP_Task_Action = $_REQUEST['RFP_Task_Action'];
		 }
		// print_r($_REQUEST);
		switch( $RFP_Task_Action ) {
			
			
			case 'AddCategory':
			
				if ( $_REQUEST['CategoryName'] != '' ) {
				
					if ( !wp_verify_nonce( $_REQUEST['_RFP_Task_addcatnonce'], 'RFP_Task_addcat') ) die( esc_html__( 'Security check failed', 'rfp-task-list' ) );
					$status = self::InsertCategory();
					
					if ( $status != 1 ) {
						$rfp_task_message = esc_html__( 'There was a problem performing that action.', 'rfp-task-list' );
						
					}
				} else {
					$rfp_task_message = esc_html__( 'Category name cannot be blank.', 'rfp-task-list' );
				}
			?>
			<script type="text/javascript" language="javascript1.5">
			alert('Task Category has been added successfully!');
			window.location='<?php echo admin_url(); ?>admin.php?page=Category-List';
			</script>
			<?php
				break;
			

			default:
				break;
		} // end switch
		

	
	
		 if (isset($_REQUEST['RFP_Task_Action']) && $_REQUEST['RFP_Task_Action'] != '') {
		 self::UpdateCategory();
		 }
		
		if (isset($_REQUEST['CheckValue']) && $_REQUEST['CheckValue'] != '') {
			self::DeleteCategory();
		}
		
	?>
	
	<script language="javascript1.5" type="text/javascript">

function checkedAll ()
{
var aa= document.getElementById('frm_content');
for (var i =0; i < aa.elements.length; i++)
{
aa.elements[i].checked = true;
}
}
function uncheckedAll ()
{
var aa= document.getElementById('frm_content');
for (var i =0; i < aa.elements.length; i++)
{
aa.elements[i].checked = false;
}
}
function check()
{
	//alert(document.getElementById('all').checked)
	if(document.getElementById('all').checked)
	{
		checkedAll ()
		
	}
	else
	{
		uncheckedAll ()
	}
}
</script>
	<?php
					if ( !taxonomy_exists('rfpcat') ) {
					self::CreateTaxonomies();
		 }
		// echo $_REQUEST['txt_id'];
					$categoryID = get_term($_REQUEST['txt_id'], 'rfpcat' );
				
						$category_id = $categoryID->term_id;
						$visibility  = get_option( 'rfpcat' );
						$visibility  = ( $visibility["category_$category_id"] != '' ? $visibility["category_$category_id"] : '0' );
				
					
					?>
					

	<div class="wrap">
	<div class="icon32"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/quote_ic.png" width="32" height="32" /></div>
	<h2><?php echo apply_filters( 'rfpcat ', esc_html__( 'Task Category', 'rfp-task-list' ) ); ?>  </h2>
	

<form name="AddRFPCat" id="AddRFPCat" action="" method="post">
			<table class="form-table">
				<tr>
					<th scope="row"><label for="rfp_task_cat_name"><?php echo apply_filters( 'RFP_Category_Name', esc_html__( 'Category Name', 'rfp-task-list' ) ); ?></label></th>
					<td><input type="text" name="CategoryName" id="CategoryName" class="regular-text" value="<?php if(isset($_REQUEST['txt_id'])) echo $categoryID->name;  ?>" style="width:300px;" required />
					<input type="hidden" name="txt_id" name="txt_id" value="<?php if (isset($_REQUEST['txt_id'])) echo $_REQUEST['txt_id']; ?>"  />
					</td>
			</tr>
			<tr>
					<th scope="row"><label for="rfp_task_cat_visibility"><?php esc_html_e( 'Assigned To', 'rfp-task-list' ); ?></label></th>
					<td>
					<?php
					if(isset($_REQUEST['txt_id'])) {
					global	$wpdb;
					$sql_sel	=	$wpdb->get_results("SELECT * FROM wp_rfp_catuser WHERE cat_id='".$_REQUEST['txt_id']."'");
					
					?>
					<select name="AssignedTo" id="AssignedTo" class="regular-text" style="width:300px;" required >
						<option value="">---SELECT ONE---</option>
						<?php
						
						 
						$users	=	get_users();
						
						foreach($users as $user) {
						
							foreach($sql_sel as $assign) {
						?>
			<option value="<?php echo $user->ID; ?>"<?php if (isset($_REQUEST['txt_id'])) if ($assign->username==$user->ID) { ?> selected="selected"<?php } ?>><?php echo esc_attr($user->user_login); ?></option>
						<?php
						}
					}
						?>
					</select>
						<?php
						} else {
						
						?>
				<select name="AssignedTo" id="AssignedTo" class="regular-text" style="width:300px;" required >
						<option value="">--SELECT ONE--</option>
						<?php
						
						 
						$users	=	get_users();
						
						foreach($users as $user) {
						
							
						?>
			<option value="<?php echo $user->ID; ?>"><?php echo esc_attr($user->user_login); ?></option>
						<?php
						}
					
						?>
					</select>
					<?php
					}
					?>
					
					</td>
			</tr>
			<tr>
				<td>
						<?php wp_nonce_field( 'RFP_Task_addcat', '_RFP_Task_addcatnonce' ); ?>
						<input type="hidden" name="RFP_Task_Action" value="<?php if (isset($_REQUEST['txt_id'])) echo "EditCategory"; else echo "AddCategory"; ?>" />
						<input type="submit" name="button" id="AddRFPTaskCategory" class="button-primary" value="<?php echo apply_filters( 'RFP_Add_Category', esc_attr__( 'Submit', 'rfp-task-list' ) ); ?>" />
					</td>
			</tr>
		</table>
		
	</form>
	<h3><?php echo apply_filters( 'RFP_Category ', esc_html__( 'Category List', 'rfp-task-list' ) ); ?></h3>
	 <form name="frm_content" id="frm_content" method="post" enctype="multipart/form-data" action="">
		<table id="rfptask-cats" class="widefat">
			<thead>
			<tr>
				<th id="id-col"><?php esc_html_e( 'S.NO', 'rfp-task-list' ); ?></th>
				<th class="row-title"><?php esc_html_e( 'Category Name', 'rfp-task-list' ); ?></th>
				<th id="vis-col"><?php esc_html_e( 'UserName', 'rfp-task-list' ); ?></th>
				<th id="action-col"><?php esc_html_e( 'Action', 'rfp-task-list' ); ?></th>
				<th id="checkbox-col" class="{sorter: false} no-sort" width="20%"><span class="icon minus"><input type="checkbox" name="all" id="all"  value="all" onClick="javascript: check();" /><input type="image" name="btn" src="<?php echo RFP_PLUGIN_URL; ?>/images/delete_but.png"></span></th>
			</tr>
			</thead>
			<tbody>
				<?php
			
				if ( !taxonomy_exists('rfpcat') ) {
				 self::CreateTaxonomies();
		 }
		// print_r($_REQUEST);
				$taxonomies	=	"rfpcat";	
				$args		=	array('orderby'=> 'term_id', 'order'=> 'DESC', 'hide_empty' => false); 
				$category 	=	get_terms( $taxonomies, $args ); //Get all categories

				if ( $category ) {
					$flag		=	1;
					global $wpdb;
					foreach ( $category as $RFPcategory ) {
					//print_r($RFPcategory);
						$category_id = $RFPcategory->term_id;
						$visibility  = get_option( 'rfpcat' );
						//print_r($visibility);
						$visibility  = ( $visibility["category_$category_id"] != '' ? $visibility["category_$category_id"] : '0' );
						
						$myrows = $wpdb->get_results("SELECT * FROM wp_rfp_catuser WHERE cat_id='".$category_id."'");
						foreach ($myrows as $user) {
						$myuser = $wpdb->get_results("SELECT * FROM wp_users WHERE ID='".$user->username."'");
						foreach ($myuser as $name) {
						?>
						
						<tr id="<?php echo $category_id; ?>">
							<td><?php echo $flag; ?></td>
							<td class="row-title"><?php echo esc_attr( $RFPcategory->name); ?></td>
							<td><?php echo esc_attr($name->user_login); ?></td>
							<td>
								<a href="javascript: void(0)" onclick="javascript: doEdit('<?php echo $RFPcategory->term_id; ?>')" title="Edit TaskList"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit.png" /></a>
								
							</td>
							<td><input type="hidden" name="CheckValue" id="CheckValue" value="Check" /><input type="checkbox" name="checkbox[]" id="checkbox[]"  value="<?php echo $RFPcategory->term_id; ?>"/></td>
						</tr>
				<?php 
				$flag++;
							}
						}
					} 
				} ?>
			</tbody>
			<tfoot>
			<tr>
				<th class="row-title"><?php esc_html_e( 'S.NO', 'rfp-task-list' ); ?></th>
				<th><?php esc_html_e('Category Name', 'rfp-task-list' ); ?></th>
				<th><?php esc_html_e( 'UserName', 'rfp-task-list' ); ?></th>
				<th><?php esc_html_e( 'Action', 'rfp-task-list' ); ?></th>
				<th class="row-title"><?php esc_html_e( 'Delete', 'rfp-task-list' ); ?></th>
			</tr>
			</tfoot>
		</table>
		</form>
		<form name="frm_edit" id="frm_edit" method="post" enctype="multipart/form-data" action="">
	<input type="hidden" name="txt_id" id="txt_id" value="" />
	
	</form>
	
					
					<script language="javascript1.5" type="text/javascript">
						function doEdit(id)
						{
							document.getElementById("txt_id").value=id;
							document.getElementById("frm_edit").submit();
							
						}
						
						</script>
	</div>
	<?php
	
		
	} 
	
 }
?>