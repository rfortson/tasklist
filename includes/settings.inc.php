<?php
/**
 * Task List Plugin Settings
 *
 * Settings the plugin
 * @author Ramona
 * @package Task-list
 * @version 1.0
 */
/**
 * Settings class
 * @package Task-list
 * @subpackage includes
 */
ob_start();
class RFP_Settings {

  function task_list_export() {
 

	// export settings
		if ( isset( $_REQUEST['file'] ) ) { 
			ob_end_clean();
			header("Content-type: text/csv");
			header("Content-Disposition: attachment;filename=myfile.csv");
			header('Pragma: no-cache');
			$general   = get_option('rfplist_general');
			$advanced  = get_option('rfplist_advanced');
			//print_r($advanced);
			$user      = get_option('rfplist_permissions');
			foreach ( $general as $id => $text)
				echo "g:$id\t".json_encode($text)."\n";
			foreach ( $advanced as $id => $text)
				echo "a:$id\t".json_encode($text)."\n";
			foreach ( $user as $id => $text)
				echo "p:$id\t".json_encode($text)."\n";
				 exit();
				 
				 
			}
		}	
	/*
	 * Renders our tabs in the plugin Task list settimgs page,
	 */
	function task_list_settings() {
	
		
	 $options = get_option( 'rfplist_general' );
	if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Save Changes') {
	
	//print_r($_REQUEST);
	
	$data	=	array('category'=>$_REQUEST['Categories'], 'priority'=>$_REQUEST['Priority'], 'date'=>$_REQUEST['ShowDate'], 'timeframe'=>$_REQUEST['TimeFrame'], 'assignedto'=>$_REQUEST['AssignedTo'], 'status'=>$_REQUEST['Status'], 'urladdress'=>$_REQUEST['URLAddress'], 'notes'=>$_REQUEST['Notes'],'wysiwyg'=>$_REQUEST['Wysiwyg']);
		RFP_Lib::general_settings($data);
	}
	?>
	<form enctype="multipart/form-data" action="" method="post">
<h3>Task List Settings</h3>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row">Categories</th>
				<td>
				<select name="Categories">
					<option value="0" <?php if(isset($options['category'])) if($options['category']==0) { echo "selected=selected"; } ?>>Disabled&nbsp;</option>
					<option value="1" <?php if(isset($options['category'])) if($options['category']==1) { echo "selected=selected"; } ?>>Enabled</option>
				</select>
		<span class="description">If you would like to organize your Task list into categories, enable it here.</span>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Priority</th>
				<td>
				<select name="Priority">
					<option value="0" <?php if(isset($options['priority'])) if($options['priority']==0) { echo "selected=selected"; } ?>>No</option>
						<option value="1" <?php if(isset($options['priority'])) if($options['priority']==1) { echo "selected=selected"; } ?>>Yes&nbsp;</option>
				</select>
	
				</td>
		</tr>
	
			<tr valign="top">
				<th scope="row">Show Date</th>
					<td>
					<select name="ShowDate">
						<option value="0" <?php if(isset($options['date'])) if($options['date']==0) { echo "selected=selected"; } ?>>No</option>
						<option value="1" <?php if(isset($options['date'])) if($options['date']==1) { echo "selected=se;ected"; } ?>>Yes&nbsp;</option>
					</select>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">Time Frame</th>
					<td>
					<select name="TimeFrame">
						<option value="0" <?php if(isset($options['timeframe'])) if($options['timeframe']==0) { echo "selected=selected"; }?>>No</option>
						<option value="1" <?php if(isset($options['timeframe'])) if($options['timeframe']==1) { echo "selected=selected"; } ?>>Yes&nbsp;</option>
					</select>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">Added By</th>
				<td>
				<select name="AssignedTo">
					<option value="0" <?php if(isset($options['assignedto'])) if($options['assignedto']==0) { echo "selected=selected"; } ?>>No</option>
					<option value="1" <?php if(isset($options['assignedto'])) if($options['assignedto']==1) { echo "selected=selected"; } ?>>Yes&nbsp;</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
				<th scope="row">Status</th>
				<td>
				<select name="Status">
					<option value="0" <?php if(isset($options['status'])) if($options['status']==0) { echo "selected=selected"; } ?>>No</option>
					<option value="1" <?php if(isset($options['status'])) if($options['status']==1)  { echo "selected=selected"; } ?>>Yes&nbsp;</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
				<th scope="row">Notes</th>
				<td>
				<select name="Notes">
					<option value="0" <?php if(isset($options['notes'])) if($options['notes']==0) { echo "selected=selected"; } ?>>No</option>
					<option value="1" <?php if(isset($options['notes'])) if($options['notes']==1) { echo "selected=selected"; } ?>>Yes&nbsp;</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">URL Address</th>
				<td>
				<select name="URLAddress">
				<option value="0" <?php if(isset($options['urladdress'])) if($options['urladdress']==0) { echo "selected=selected"; } ?>>No</option> 
					<option value="1" <?php if(isset($options['urladdress'])) if($options['urladdress']==1) { echo "selected=selected"; } ?>>Yes&nbsp;</option>
					
				</select>
				</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">Use WYSIWYG Editor</th>
			<td>
			<select name="Wysiwyg">
				<option value="0" <?php if(isset($options['wysiwyg'])) if($options['wysiwyg']==0) { echo "selected=selected"; } ?>>No</option>
				<option value="1" <?php if(isset($options['wysiwyg'])) if($options['wysiwyg']==1) { echo "selected=selected"; } ?>>Yes&nbsp;</option>
			</select>
			</td>
		</tr>
		
		
	</tbody>
</table>								
<p class="submit"><input type="submit" value="Save Changes" class="button-primary" id="submit" name="submit"></p>
</form>
	<?php
	}
	/*
	 * Renders our tabs in the plugin Task list advanced settimgs page,
	 */
	function task_list_advanced() {
	
	 $advanced = get_option( 'rfplist_advanced' );
	 
	 //print_r($advanced);
	
	if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Save Changes') {
		
		$data		=	array('date_format'=> $_REQUEST['DateFormat'],'taskdisplay' => $_REQUEST['taskdisplay'], 'priority_0'=> $_REQUEST['HighestPriority'], 'priority_1'=> $_REQUEST['MiddlePriority'], 'priority_2'=> $_REQUEST['LowerPriority'], 'show_id'=> $_REQUEST['ShowID'], 'show_date_added'=> $_REQUEST['ShowDateAdded'],'ShowRecurring'=>$_REQUEST['ShowRecurring'],'RecurringTask' => $_REQUEST['RecurringTask'], 'assign'=> $_REQUEST['AssignUsers'], 'show_only_assigned'=> $_REQUEST['ShowOnlyAssigned'], 'user_roles'=> array(0=> $_REQUEST['UserRoles_1'], 1=> $_REQUEST['UserRoles_2'], 2=> $_REQUEST['UserRoles_3'], 3=> $_REQUEST['UserRoles_4'], 4=> $_REQUEST['UserRoles_5']), 'email_assigned'=> $_REQUEST['EmailAssigned'], 'email_category'=> $_REQUEST['EmailCategory'], 'email_show_assigned_by'=> $_REQUEST['EmailShowAssignedBy'], 'email_from'=> $_REQUEST['EmailFrom'], 'email_subject'=> $_REQUEST['EmailSubject'], 'email_text'=> $_REQUEST['EmailText']);
		
		RFP_Lib::advanced_settings($data);
	}
	?>
	<form enctype="multipart/form-data" action="" method="post">
<h3>Task List Advanced Settings</h3>
<strong>Customize the Task List</strong>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row">Date Format</th>
				<td><input type="text" value="<?php if (isset($advanced['date_format'])) echo $advanced['date_format']; ?>" name="DateFormat" class="small-text"><br>
				<a href="http://codex.wordpress.org/Formatting_Date_and_Time">Documentation on Date Formatting</a>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Default Task Display</th>
				<td>
				<input type="text" name="taskdisplay" id="taskdisplay" value="<?php if (isset($advanced['taskdisplay'])) echo $advanced['taskdisplay'];  ?>" />
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Highest Priority Label</th>
				<td><input type="text" value="<?php if (isset($advanced['priority_0'])) echo $advanced['priority_0']; ?>" name="HighestPriority">
				<span class="description">The highest priority list items are shown in red in the lists.</span>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Middle Priority Label</th>
				<td>
				<input type="text" value="<?php if (isset($advanced['priority_1'])) echo $advanced['priority_1']; ?>" name="MiddlePriority">
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Lowest Priority Label</th>
				<td>
				<input type="text" value="<?php if (isset($advanced['priority_2'])) echo $advanced['priority_2']; ?>" name="LowerPriority">
				<span class="description">The lowest priority list items are shown in a lighter grey.</span>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Show Task Item ID</th>
				<td>
				<select name="ShowID">
					<option value="0" <?php if (isset($advanced['show_id'])) if ($advanced['show_id']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
					<option value="1" <?php if (isset($advanced['show_id'])) if ($advanced['show_id']==1) { echo "selected=selected"; } ?>>Yes</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Show Date Task Was Added</th>
				<td>
				<select name="ShowDateAdded">
					<option value="0" <?php if (isset($advanced['show_date_added'])) if ($advanced['show_date_added']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
					<option value="1" <?php if (isset($advanced['show_date_added'])) if ($advanced['show_date_added']==1) { echo "selected=selected"; } ?>>Yes</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Show Recurring Task</th>
				<td>
				<select name="ShowRecurring">
					<option value="0" <?php if (isset($advanced['ShowRecurring'])) if ($advanced['ShowRecurring']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
					<option value="1" <?php if (isset($advanced['ShowRecurring'])) if ($advanced['ShowRecurring']==1) { echo "selected=selected"; } ?>>Yes</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Recurring Task List</th>
				<td>
				<select name="RecurringTask">
					<option value="daily" <?php if (isset($advanced['RecurringTask'])) if ($advanced['RecurringTask']=='daily') { echo "selected=selected"; } ?>>Daily</option>
					<option value="weekly" <?php if (isset($advanced['RecurringTask'])) if ($advanced['RecurringTask']=='weekly') { echo "selected=selected"; } ?>>Weekly</option>
					<option value="twicemonth" <?php if (isset($advanced['RecurringTask'])) if ($advanced['RecurringTask']=='twicemonth') { echo "selected=selected"; } ?>>Twice a month</option>
					<option value="monthly" <?php if (isset($advanced['RecurringTask'])) if ($advanced['RecurringTask']=='monthly') { echo "selected=selected"; } ?>>Monthly on</option>
				</select>
				
				
				</td>
		</tr>
	</tbody>
</table>
<h3>Assign Task Items Settings (Only When Using Group or Master View)</h3>
<strong>Configure these settings to be able to assign Task items to other users.</strong>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row">Assign Task Items to Users</th>
				<td>
					<select name="AssignUsers">
						<option value="0" <?php if (isset($advanced['assign'])) if ($advanced['assign']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
						<option value="1" <?php if (isset($advanced['assign'])) if ($advanced['assign']==1) { echo "selected=selected"; }  ?>>Yes</option>
					</select>
					<span class="description">This setting must be set to Yes for the following settings to work.</span>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">Show a User Only the Task Items Assigned to Them</th>
				<td>
				<select name="ShowOnlyAssigned">
					<option value="0" <?php if (isset($advanced['show_only_assigned'])) if ($advanced['show_only_assigned']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
					<option value="1" <?php if (isset($advanced['show_only_assigned'])) if ($advanced['show_only_assigned']==1) { echo "selected=selected"; } ?>>Yes</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row">User Roles to Show</th>
				<td><input type="checkbox" value="administrator" <?php if (isset($advanced['user_roles'][0])) if ($advanced['user_roles'][0]=='administrator') { echo 'checked="checked"'; } ?> name="UserRoles_1"> Administrator &nbsp; <input type="checkbox" value="editor" <?php if (isset($advanced['user_roles'][1])) if ($advanced['user_roles'][1]=='editor') { echo 'checked="checked"';} ?> name="UserRoles_2"> Editor &nbsp; <input type="checkbox" value="author" <?php if (isset($advanced['user_roles'][2])) if ($advanced['user_roles'][2]=='author') { echo 'checked="checked"'; } ?> name="UserRoles_3"> Author &nbsp; <input type="checkbox" value="contributor" <?php if (isset($advanced['user_roles'][3])) if ($advanced['user_roles'][3]=='contributor') { echo 'checked="checked"'; } ?> name="UserRoles_4"> Contributor &nbsp; <input type="checkbox" value="subscriber" <?php if (isset($advanced['user_roles'][4])) if ($advanced['user_roles'][4]=='subscriber') { echo 'checked="checked"'; } ?> name="UserRoles_5"> Subscriber &nbsp; 		<br><span class="description">Used in displaying the list of users who can be assigned RFP-Task items.</span><br>
		<a href="http://codex.wordpress.org/Roles_and_Capabilities">Documentation on User Roles</a>
				</td>
		</tr>
			<tr valign="top">
				<th scope="row">Email Assigned Task Items to User</th>
					<td>
					<select name="EmailAssigned">
						<option value="0" <?php if (isset($advanced['email_assigned'])) if ($advanced['email_assigned']==0) { echo  "selected=selected"; } ?>>No&nbsp;</option>
						<option value="1" <?php if (isset($advanced['email_assigned'])) if ($advanced['email_assigned']==1) { echo  "selected=selecetd"; } ?>>Yes</option>
					</select>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">Add Category to Subject</th>
					<td>
					<select name="EmailCategory">
						
						<option value="0" <?php if (isset($advanced['email_category'])) if ($advanced['email_category']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
							<option value="1" <?php if (isset($advanced['email_category'])) if ($advanced['email_category']==1) { echo "selected=selected"; } ?> >Yes</option>
					</select>
					<span class="description">If categories are enabled.</span>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">Show Who Assigned the Task Item in Email</th>
					<td>
						<select name="EmailShowAssignedBy">
							<option value="0" <?php if (isset($advanced['email_show_assigned_by'])) if ($advanced['email_show_assigned_by']==0) { echo "selected=selected"; } ?>>No&nbsp;</option>
							<option value="1" <?php if (isset($advanced['email_show_assigned_by'])) if ($advanced['email_show_assigned_by']==1) { echo "selected=selected"; } ?> >Yes</option>
						</select>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">From Field for Emails Sent to User</th>
					<td>
					<input type="text" value="<?php if (isset($advanced['email_from'])) echo $advanced['email_from']; ?>" name="EmailFrom" class="regular-text">
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">Subject Field for Emails Sent to User</th>
					<td>
					<input type="text" value="<?php if (isset($advanced['email_subject'])) echo $advanced['email_subject']; ?>" name="EmailSubject" class="regular-text">
					</td>
			</tr>
			<tr valign="top">
				<th scope="row">Text in Emails Sent to User</th>
				<td>
				<textarea cols="70" rows="3" name="EmailText"><?php if (isset($advanced['email_text'])) echo $advanced['email_text']; ?></textarea>
				</td>
			</tr>
		</tbody>
	</table>					
<p class="submit"><input type="submit" value="Save Changes" class="button-primary" id="submit" name="submit"></p>	
	</form>
	<?php
	}
	/*
	 * Renders our tabs in the plugin Task list userpermissions page,
	 */
	function task_list_userpermissions() {
	
	
	 $permissions = get_option( 'rfplist_permissions' );
	 
	// print_r($permissions);
	
	 
	 if (isset($_REQUEST) && $_REQUEST['submit']=='Save Changes') {
	 
	 	$data		=	array('view_capability'=> $_REQUEST['ViewCapability'], 'add_capability'=> $_REQUEST['AddCapability'], 'edit_capability'=> $_REQUEST['EditCapability'], 'delete_capability'=> $_REQUEST['DeleteCapability'], 'purge_capability'=> $_REQUEST['PurgeCapability'], 'complete_capability'=>$_REQUEST['CompleteCapability'], 'assign_capability'=>$_REQUEST['AssignCapability'], 'view_all_assigned_capability'=> $_REQUEST['ViewAllAssignedCcapability'], 'add_cat_capability'=> $_REQUEST['AddCatCapability']);
		
		RFP_Lib::permissions_settongs($data);
	 }
	 
	?>
	<form enctype="multipart/form-data" action="" method="post">
<h3>Task List User Permissions for Group and Master List Types</h3>
<strong>The only permission that applies to the Individual list type is the View Task List permission.</strong><br><br>You should chose the highest level capabilities that the users you want to be able to preform that action will have.<br><br>The default general capabilities of each user role are as follows: <br>Subscribers: Read, Contributors: Edit Posts, Authors: Publish Posts, Editors: Edit Others Posts, Administrators: Manage Options<br><br><em><strong>When using the Master list type non-administrator users should only be allowed to view and complete items, otherwise they will be able to edit the Master list.</strong></em>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row"><label for="view_capability">View Task List</label></th>
				<td>
				<select name="ViewCapability">
					<option value="read" <?php if (isset($permissions['view_capability'])) if ($permissions['view_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
					<option value="edit_posts"<?php if (isset($permissions['view_capability'])) if ($permissions['view_capability']=='edit_posts') { echo "selected=selected"; } ?> >Edit Posts</option>
					<option value="publish_posts" <?php if(isset($permissions['view_capability'])) if ($permissions['view_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
					<option value="edit_others_posts" <?php if(isset($permissions['view_capability'])) if ($permissions['view_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
			<option value="publish_pages" <?php if (isset($permissions['view_capability'])) if ($permissions['view_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
					<option value="edit_users" <?php if (isset($permissions['view_capability'])) if ($permissions['view_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
					<option value="manage_options" <?php if (isset($permissions['view_capability'])) if ($permissions['view_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
				</select>
				</td>
		</tr>
			<tr valign="top">
				<th scope="row"><label for="complete_capability">Complete Task Item Capability</label></th>
					<td>
					<select name="CompleteCapability">
						<option value="read" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='read') { echo "selecetd=selected"; } ?>>Read</option>
						<option value="edit_posts" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
						<option value="publish_posts" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
						<option value="edit_others_posts" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
						<option value="publish_pages" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
						<option value="edit_users" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
						<option value="manage_options" <?php if (isset($permissions['complete_capability'])) if ($permissions['complete_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
					</select>
					</td>
			</tr>
				<tr valign="top">
					<th scope="row"><label for="add_capability">Add Task Item Capability</label></th>
						<td>
						<select name="AddCapability">
							<option value="read" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
							<option value="edit_posts" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='edit_posts') { echo "selected"; } ?>>Edit Posts</option>
							<option value="publish_posts" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
							<option value="edit_others_posts" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
							<option value="publish_pages" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
							<option value="edit_users" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
							<option value="manage_options" <?php if (isset($permissions['add_capability'])) if ($permissions['add_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
						</select>
						</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="edit_capability">Edit Task Item Capability</label></th>
						<td>
						<select name="EditCapability">
							<option value="read" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
							<option value="edit_posts" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
							<option value="publish_posts" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
							<option value="edit_others_posts" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='edit_other_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
							<option value="publish_pages" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
							<option value="edit_users" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
							<option value="manage_options" <?php if (isset($permissions['edit_capability'])) if ($permissions['edit_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
					</select>
					</td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="assign_capability">Assign Task Item Capability</label></th>
				<td>
				<select name="AssignCapability">
					<option value="read" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
					<option value="edit_posts" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
					<option value="publish_posts" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
					<option value="edit_others_posts" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
					<option value="publish_pages" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
					<option value="edit_users" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
					<option value="manage_options" <?php if (isset($permissions['assign_capability'])) if ($permissions['assign_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="view_all_assigned_capability">View Task Items Assigned to Other Users Capability</label></th>
				<td>
				<select name="ViewAllAssignedCcapability">
					<option value="read" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
					<option value="edit_posts" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
					<option value="publish_posts" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
					<option value="edit_others_posts" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
					<option value="publish_pages" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
					<option value="edit_users" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
					<option value="manage_options" <?php if (isset($permissions['view_all_assigned_capability'])) if ($permissions['view_all_assigned_capability']=='manage_options') { echo "selected=selected"; }  ?>>Manage Options</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="delete_capability">Delete Task Item Capability</label></th>
				<td>
				<select name="DeleteCapability">
					<option value="read" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
					<option value="edit_posts" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
					<option value="publish_posts" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
					<option value="edit_others_posts" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
					<option value="publish_pages" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
					<option value="edit_users" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
					<option value="manage_options" <?php if (isset($permissions['delete_capability'])) if ($permissions['delete_capability']=='manage_options') { echo "selected=selected"; } ?> >Manage Options</option>
				</select>
				</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="purge_capability">Delete All Task Items Capability</label></th>
				<td>
				<select name="PurgeCapability">
					<option value="read" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
					<option value="edit_posts" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
					<option value="publish_posts" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
					<option value="edit_others_posts" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
					<option value="publish_pages" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
					<option value="edit_users" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
					<option  value="manage_options" <?php if (isset($permissions['purge_capability'])) if ($permissions['purge_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
				</select>
				</td>
		</tr>
			<tr valign="top">
				<th scope="row"><label for="add_cat_capability">Add Categories Capability</label></th>
					<td>
					<select name="AddCatCapability">
						<option value="read" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='read') { echo "selected=selected"; } ?>>Read</option>
						<option value="edit_posts" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='edit_posts') { echo "selected=selected"; } ?>>Edit Posts</option>
						<option value="publish_posts" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='publish_posts') { echo "selected=selected"; } ?>>Publish Posts</option>
						<option value="edit_others_posts" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='edit_others_posts') { echo "selected=selected"; } ?>>Edit Others Posts</option>
						<option value="publish_pages" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='publish_pages') { echo "selected=selected"; } ?>>Publish Pages</option>
						<option value="edit_users" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='edit_users') { echo "selected=selected"; } ?>>Edit Users</option>
						<option value="manage_options" <?php if (isset($permissions['add_cat_capability'])) if ($permissions['add_cat_capability']=='manage_options') { echo "selected=selected"; } ?>>Manage Options</option>
					</select>
					</td>
			</tr>
	</tbody>
</table>
	<p class="submit"><input type="submit" value="Save Changes" class="button-primary" id="submit" name="submit"></p>		</form>
	<?php
	}
	/*
	 * Renders our tabs in the plugin Task list import/export page,
	 */
	function task_list_importexport() {
	
	
	

	
	?>
	<form enctype="multipart/form-data" action="" method="post">
<h3>Import/Export Settings</h3>

<!--<p><a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=importexport&file=export" class="submit button">Download Export File</a></p>-->
<p><a href="<?php echo admin_url(); ?>admin.php?page=my-menu&actions=download" class="submit button">Download Export File</a></p>


	<p>
		<label for="RFP-Task-list-settings-import-file">Choose File to Import:</label>
		<input type="file" id="ImportFile" name="ImportFile">
		<input type="submit" value="Import" class="button" id="settings-submit" name="import_submit">	</p>

		<h3>Import/Export Task Items</h3>

		<p>You can use the built-in WordPress Import/Export feature, located under the Tools menu. On the Export screen, clicking the Download Export File button.</p>

</form>
	<?php
	 if ($_REQUEST['import_submit']=='Import') {
	 
	 		if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
				global $wpdb;
				$uploadedfile 	=	$_FILES['ImportFile']['name'];
				$uploaded		=	$_FILES['ImportFile'];
				$upload_dir = wp_upload_dir();
				
				$wp_filetype 	=	wp_check_filetype($uploadedfile);
				//print_r($wp_filetype['ext']); exit;
					if ($wp_filetype['ext']=='csv') {
					$upload_overrides = array( 'test_form' => false );
					$movefile 		  = wp_handle_upload( $uploaded, $upload_overrides );
					$handle 		  =	fopen($upload_dir['path']."/$uploadedfile", "r");
					while (($data 	  =	fgetcsv($handle, 1000, ",")) !== FALSE)
					 {
					 	//print_r($data);die();
						$my_post = array(
				'post_type'        => 'rfptask',
				'post_title'       => $data[0],
				'post_content'     => $data[9],
				'post_status'      => 'publish',
				'post_author'      => $data[4],
				'post_excerpt'	   => $data[2],
				'comment_status'   => 'closed',
				'ping_status'      => 'closed',
			);
					 $post_id 	= wp_insert_post( $my_post );
					  $sql_ins	= $wpdb->query("INSERT INTO wp_term_relationships(object_id,term_taxonomy_id) VALUES('".$post_id."','".$data[2]."')"); 
					 
			 $advanced = get_option( 'rfplist_advanced' );
		     $assign		=	$data[4];
			
				if ( isset( $data[2] ) ) wp_set_post_terms( $post_id, absint( $data[2] ), 'RFP_Category', false);
			add_post_meta( $post_id, '_status', 0, true );
			$priority = ( isset( $data[1] ) ?  $data[1]  : 'Low' );
			add_post_meta( $post_id, '_priority', $priority, true );
			$user_info = get_userdata($data[4]);
			$first_name = $user_info->user_login;
			//exit;
			$assign 	= ( isset( $first_name ) ? $first_name : -1 );
			if ( is_array( $assign ) ) {
					foreach ( $assign as $value ) {
						add_post_meta( $post_id, '_assign', $value );
					}
				} else {
					add_post_meta( $post_id, '_assign', $assign );
				}
			$date = ( isset( $data[8] ) && $data[8] != '' ?  $data[8]  : '' );
			add_post_meta( $post_id, '_Date', $date, true );
			$timeframe = ( isset( $data[3] ) ? $data[3] : 0 );
			add_post_meta( $post_id, '_TimeFrame', $timeframe, true );
			if ( isset( $data[5] ) ) add_post_meta( $post_id, '_rfpstatus', $data[5] );
			add_post_meta( $post_id, '_rfpstatus', $rfpstatus, true );
			if ( isset( $data[6] ) ) add_post_meta( $post_id, '_notes', esc_attr( $data[6] ) );
			add_post_meta( $post_id, '_notes', $notes, true );
			if ( isset( $data[7] ) ) add_post_meta( $post_id, '_urladdress', esc_attr( $data[7] ) );
			add_post_meta( $post_id, '_urladdress', $urladdress, true );
			$current_user = wp_get_current_user();
	  		add_post_meta( $post_id, '_addedto', esc_attr( $current_user->user_login ) );
			$postdate	=	time();
			$dates		=	date("m/d/Y",$postdate);
			add_post_meta( $post_id, '_postdate', esc_attr($dates));

					   
					 }

				 fclose($handle);
			
				
				//print_r($movefile);die();
				if ( $movefile ) {
				?>
				<script type="text/javascript" language="javascript1.5">
				alert("File is valid, and was successfully uploaded");
				window.location='<?php echo admin_url(); ?>admin.php?page=Settings&tab=importexport';
				</script>
				<?php
					
					//var_dump( $movefile);
				} else {
				?>
				<script type="text/javascript" language="javascript1.5">
				alert("Possible file upload attack!");
				window.location='<?php echo admin_url(); ?>admin.php?page=Settings&tab=importexport';
				</script>
				<?php
					
				}
	 		} else {
			?>
			<script type="text/javascript" language="javascript1.5">
			alert("Invalid File Type");
			window.location='<?php echo admin_url(); ?>admin.php?page=Settings&tab=importexport';
			</script>
			<?php
			
			}
	 }
	
	
	}
	function task_list_help() {
	?>
	<div class="active help-tab-content">
										<h3>Task List</h3>
		<p>This plugin provides users with a Task list feature. You can configure the plugin to have private Task lists for each user, for all users to share a Task list, or a master list with individual completing of items. The shared Task list has a variety of settings available. You can assign Task items to a specific user (includes a setting to email a new Task item to the assigned user) and have only those Task items assigned viewable to a user.Category support is included as well as front-end.</p>
		<p>A new menu item is added to manage your list and it is also listed on a dashboard widget. A shortcode to display the Task list items on your site.</p>
								</div>
	<?php
	}
	function task_list_shortcodes() {
	
	?>
	<div class="active help-tab-content">
										<h3>Shortcode Documentation</h3>
		<h4>Display a List of Task Items</h4>
		<p><strong>[TaskList]</strong></p>

		<ul>
			<li><strong>title</strong> &ndash; you can give title</em> (Ex: [TaskListDisplay title="Task List"]).</li>
			<li><strong>Category</strong>&ndash; default is <em>show</em></li>
			<li><strong>Taskname</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [TaskListDisplay taskname="hide"]).</li>
			<li><strong>AssignedTo</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [TaskListDisplay assignedto="hide"]).</li>
			<li><strong>AddedBy</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [TaskListDisplay addedby="hide"]).</li>
			<li><strong>Due Date</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [TaskListDisplay duedate="hide"]).</li>
			<li><strong>Status</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [TaskListDisplay status="hide"]).</li>
			<li><strong>Priority</strong>&ndash; default is hide. Use a show value to show. (Ex: [TaskListDisplay priority="show"]).</li>
			<li><strong>Time Frame</strong>&ndash; default is hide. Use a show value to show. (Ex: [TaskListDisplay timeframe="show"]).</li>
			<li><strong>list_type</strong> &ndash; default is <em>table view</em> . Use <em>ol</em> to show an ordered list. (Ex: [TaskListDisplay list_type="ol"]).</li>
			
		</ul>

		<p><strong>Example:</strong><br>
		Table view with the title of Task List and showing the priority and hide the task item.<br>
		[TaskListDisplay title="Task List" type="table" taskname="hide" Priority="show"]</p>

		<hr>

		<h4>Display a Checklist of Task Items</h4>
		<p><strong>[CheckList]</strong></p>


		<ul>
			<li><strong>title</strong> &ndash; you can give title</em> (Ex: [CheckListDisplay title="Task List"]).</li>
			<li><strong>Category</strong>&ndash; default is <em>show</em></li>
			<li><strong>Taskname</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [CheckListDisplay taskname="hide"]).</li>
			<li><strong>AssignedTo</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [CheckListDisplay assignedto="hide"]).</li>
			<li><strong>AddedBy</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [CheckListDisplay addedby="hide"]).</li>
			<li><strong>Due Date</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [CheckListDisplay duedate="hide"]).</li>
			<li><strong>Status</strong>&ndash; default is <em>show</em>. Use a hide value to hide (Ex: [CheckListDisplay status="hide"]).</li>
			<li><strong>Priority</strong>&ndash; default is hide. Use a show value to show. (Ex: [CheckListDisplay priority="show"]).</li>
			<li><strong>Time Frame</strong>&ndash; default is hide. Use a show value to show. (Ex: [CheckListDisplay timeframe="show"]).</li>
					
		</ul>
		<p><strong>Example:</strong><br>
		<strong>(You must be logged in to view)</strong><br />
		Table view with the title of Task List and showing the priority and hide the task item.<br>
		[CheckListDisplay title="Task List" type="table" taskname="hide" Priority="show"]</p>


	
		
									</div>
	<?php
	}
	function task_list_faqs() {
	?>
	<div class="active help-tab-content">
										<h3>Frequently Asked Questions</h3>

		<p><strong>What should I do if I find a bug?</strong><br>
		Visit the plugin website and leave a comment or contact me.<br>
		<a target="_blank" href="http://rfwebsiteprogramming.com/plugins/TaskList/">http://rfwebsiteprogramming.com/plugins/TaskList/</a><br>
		<a target="_blank" href="http://rfwebsiteprogramming.com/contact-us/">http://rfwebsiteprogramming.com/contact-us/</a>
		</p>
								</div>
	<?php
	}
	/*
	  * plugin_options_page method.
	 */
	function plugin_options_tabs() {
	

	}
 
}
?>