<?php
/**
 * Task List Plugin Loader
 *
 * Loads the plugin
 * @author Ramona
 * @package Task-list
 * @version 1.0
 */

/**
 * Loader class
 * @package Task-list
 * @subpackage includes
 */
if (isset($_REQUEST['bulk_edit']) && $_REQUEST['bulk_edit']!='') {
 add_action( 'init', 'bulktaskaction' );
}

function bulktaskaction() {
global $wpdb;
//print_r($_REQUEST);
if(isset($_REQUEST['task_name']) && $_REQUEST['task_name']!='') {
foreach($_REQUEST['task_name'] as $taskname) {

 update_post_meta( $taskname, '_rfpstatus', $_REQUEST['Status'] );
 $user_info = get_userdata($_REQUEST['AssignedTo']);
 //print_r($user_info);die();
		$first_name = $user_info->user_login;
		$assign 	= $first_name;
		//print_r($assign);
		if ( is_array( $assign ) ) {
					foreach ( $assign as $value ) {
						update_post_meta($taskname, '_assign', $value );
					}
				} else {
					update_post_meta($taskname, '_assign', $assign );
				}
		
		 $sql_update	= $wpdb->query("UPDATE wp_term_relationships SET term_taxonomy_id='".$_REQUEST['Category']."' WHERE  object_id='$taskname'");
		//print_r($sql_update);
}
?>
<script type="text/javascript" language="javascript1.5">
alert('Task List has been updated successfully!');
window.location='<?php echo admin_url(); ?>admin.php?page=my-menu';
</script>
<?php
	
} else {
?>
<script type="text/javascript" language="javascript1.5">
alert('Please select atleast one task Items!');
window.location='<?php echo admin_url(); ?>admin.php?page=my-menu';
</script>
<?php
	}
}
class RFP_Loader {

	
	

/**
	 * Loads the CSS file for the WP backend
	 * @static
	 */
	public static function add_admin_css() {
		$RFP_style_url =  RFP_PLUGIN_URL.'/css/RFP-task-list-admin.css';
		$RFP_style_file = RFP_PLUGIN_DIR.'/css/RFP-task-list-admin.css';
		
		
		if ( file_exists( $RFP_style_file ) ) {
			wp_register_style( 'RFP_task_style_sheet', $RFP_style_url, array(), RFP_PLUGIN_VERSION );
			wp_enqueue_style( 'RFP_task_style_sheet' );
		
			
		}
	}

/**
	 * Loads and localizes JS files for the WP backend
	 * @static
	 */
	public static function add_admin_js() {
	  
		//wp_register_script( 'RFP_latest_js', RFP_PLUGIN_URL.'/js/jquery-1.7.2.min.js', '', RFP_PLUGIN_VERSION, true );
		wp_register_script( 'RFP_task_js', 'http://code.jquery.com/ui/1.10.1/jquery-ui.js', '', RFP_PLUGIN_VERSION, true );
		wp_register_script( 'RFP_metadata_js', RFP_PLUGIN_URL.'/js/jquery.metadata.js', '', RFP_PLUGIN_VERSION, true );
		wp_register_script( 'RFP_tablesorter_js', RFP_PLUGIN_URL.'/js/jquery.tablesorter.min.js', '', RFP_PLUGIN_VERSION, true );
	//	wp_enqueue_script(  'RFP_latest_js' );
		wp_enqueue_script(  'RFP_task_js' );
		wp_enqueue_script(  'RFP_metadata_js' );
		wp_enqueue_script(  'RFP_tablesorter_js' );
		wp_enqueue_script(  'jquery-color' );
		wp_enqueue_script(  'jquery-ui-datepicker' );
		wp_enqueue_script(  'jquery-ui-core' );
		wp_enqueue_script(  'jquery-ui-slider' );
		wp_enqueue_script(  'jquery-ui-widget' );
		wp_enqueue_script(  'jquery-ui-mouse' );
		wp_enqueue_script(  'jquery-ui-resizable' );
		wp_localize_script( 'RFP_task_js', 'tl', RFP_Loader::get_js_vars() );
	}
	/**
	 * Localize JS variables
	 * @static
	 * @return array
	 */
	public static function get_js_vars() {
		return array(
			'SUCCESS_MSG'                 => __( 'Task Deleted.', 'RFP-task-list' ),
			'ERROR_MSG'                   => __( 'There was a problem performing that action.', 'RFP-task-list' ),
			'PERMISSION_MSG'              => __( 'You do not have sufficient privileges to do that.', 'RFP-task-list' ),
			'CONFIRMATION_MSG'            => __( "You are about to permanently delete the selected item. \n 'Cancel' to stop, 'OK' to delete.", 'RFP-task-list' ),
			'CONFIRMATION_ALL_MSG'        => __( "You are about to permanently delete all completed items. \n 'Cancel' to stop, 'OK' to delete.", 'RFP-task-list' ),
			'CONFIRMATION_DELETE_ALL_MSG' => __( "You are about to permanently delete all task. \n 'Cancel' to stop, 'OK' to delete.", 'RFP-task-list' ),
			'CONFIRMATION_DEL_TABLES_MSG' => __( "You are about to permanently delete database tables. This cannot be undone. \n 'Cancel' to stop, 'OK' to delete.", 'RFP-task-list' ),
			'SELECT_USER'                 => __( 'Select a User', 'RFP-task-list' ),
			'NONCE'                       => wp_create_nonce( 'RFP-Task' ),
			'AJAX_URL'                    => admin_url( 'admin-ajax.php' )
		);
	}
	public static function UpdateTask1() {
	//print_r($_REQUEST);
		
		 $id		=	absint($_REQUEST['txt_rid']);
		 if ( $_REQUEST['TaskDescription'] != '' ) {
		global $wpdb;
		//print_r($_REQUEST);
		$my_post = array(
				'ID'			   => $id,
				'post_type'        => 'rfptask',
				'post_title'       => $_REQUEST['TaskName'],
				'post_content'     => $_REQUEST['TaskDescription'],
				'post_status'      => 'publish',
				'post_author'      => $_REQUEST['AssignedTo'],
				'post_excerpt'	   => $_REQUEST['CategoryName'],
				'comment_status'   => 'closed',
				'ping_status'      => 'closed',
			);
		 $post_id 	 =  wp_update_post( $my_post );
		 $cat_update =  wp_update_term($post_id , $_REQUEST['CategoryName']);
		// print_r($cat_update);
		// die();
		 update_post_meta($post_id, '_status', '0');
		 $priority = ( isset( $_REQUEST['Priority'] ) ?  $_REQUEST['Priority']  : 'Low' );
		 update_post_meta( $post_id, '_priority', $priority, true );
		 $user_info = get_userdata($_REQUEST['AssignedTo']);
		//print_r($user_info);die();
		$first_name = $user_info->user_login;
		$assign 	= $first_name;
		//print_r($assign);
		if ( is_array( $assign ) ) {
					foreach ( $assign as $value ) {
						update_post_meta($post_id, '_assign', $value );
					}
				} else {
					update_post_meta($post_id, '_assign', $assign );
				}
			$date = ( isset( $_REQUEST['datepicker'] ) && $_REQUEST['datepicker'] != '' ?  $_REQUEST['datepicker']  : '' );
			update_post_meta( $post_id, '_Date', $date );
			$timeframe = ( isset( $_REQUEST['TimeFrame'] ) ? $_REQUEST['TimeFrame'] : 0 );
			update_post_meta( $post_id, '_TimeFrame', $timeframe );
			if ( isset( $_REQUEST['Status'] ) ) update_post_meta( $post_id, '_rfpstatus', $_REQUEST['Status'] );
			if ( isset( $_REQUEST['Notes'] ) ) update_post_meta( $post_id, '_notes', esc_attr( $_REQUEST['Notes'] ) );
			if ( isset( $_REQUEST['UrlAddress'] ) ) update_post_meta( $post_id, '_urladdress', esc_attr( $_REQUEST['UrlAddress'] ) );
			if ( isset( $_REQUEST['Recurring'])) update_post_meta($post_id, '_recurring', $_REQUEST['Recurring']);
			
			?>
		<script type="text/javascript"  language="javascript1.5">
		alert('Task has been updated successfully!');
		window.location='<?php echo admin_url(); ?>index.php';
		</script>
		<?php
		} else {
			return;
		}
	
	 }
	public static function updatenotes1() {
 	$notes_id		=	absint($_REQUEST['txt_notes']);
	if ( $_REQUEST['Notes'] != '' ) {
		//print_r($_REQUEST);die();
		update_post_meta($notes_id, '_notes', esc_attr( $_REQUEST['Notes'] ) );
		?>
		<script type="text/javascript"  language="javascript1.5">
		alert('Task Notes has been updated successfully!');
		window.location='<?php echo admin_url(); ?>index.php';
		</script>
				<?php
		}
	}
	function dashboard_widget() {
	
	$options	 = get_option( 'rfplist_general' );
	   $advanced = get_option( 'rfplist_advanced' );
	  // print_r($advanced);
	global $wpdb;
	$options1 = $wpdb->get_results("SELECT * FROM wp_rfpscreenoptions WHERE id = 1");
	foreach($options1 as $screen)  {
	//print_r($screen);
	$priority	=	 $screen->priority;
	$duedate	=	 $screen->duedate;
	$assigned	=	 $screen->assigned;
	
	
	
	}
		if (isset($_REQUEST['CheckValue']) && $_REQUEST['CheckValue'] != '' && $_REQUEST['action']=='edit') {
			//self::DeleteTask();
			self::bulkactions1();
			//exit;
		}
			if ($_REQUEST['action']=='trash') {
	RFP_Loader::tasklisttrash();
	}if (isset($_REQUEST['CheckValue']) && $_REQUEST['CheckValue'] != '' && isset($_REQUEST['checkbox']) && $_REQUEST['action']=='trash') {
			
			RFP_Loader::tasklisttrash();
			//exit;
			
		}
			if(isset($_REQUEST['txt_rfpid'])) {
			RFP_Tasklist::new_tasklist();
			
			}
			if (isset($_REQUEST['TaskName']) && $_REQUEST['TaskName']!='') {
			self::UpdateTask1();
			}
			if (isset($_REQUEST['txt_notes']) && $_REQUEST['txt_notes']!='') {
			RFP_Tasklist::Notes();
			}
			if (isset($_REQUEST['Notes']) && $_REQUEST['Notes']!='') {
			self::updatenotes1();
			}
			?>

		
	<div class="wrap" >
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
		 <link rel="stylesheet" href="<?php echo RFP_PLUGIN_URL; ?>/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
		<script src="<?php echo RFP_PLUGIN_URL; ?>/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	 <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$("area[rel^='prettyPhoto']").prettyPhoto();
				
				$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: true});
				$(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
		
				$("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
					custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
					changepicturecallback: function(){ initialize(); }
				});

				$("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
					custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
					changepicturecallback: function(){ _bsap.exec(); }
				});
			});
			</script>
		<script language="javascript1.5" type="text/javascript">

function checkedAll ()
{
var aa= document.getElementById('frm_content');
for (var i =0; i < aa.elements.length; i++)
{
aa.elements[i].checked = true;
}
}
function uncheckedAll ()
{
var aa= document.getElementById('frm_content');
for (var i =0; i < aa.elements.length; i++)
{
aa.elements[i].checked = false;
}
}
function check()
{
	//alert(document.getElementById('all').checked)
	if(document.getElementById('all').checked)
	{
		checkedAll ()
		
	}
	else
	{
		uncheckedAll ()
	}
}

</script>

	   <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
     <!-- datepicker javascript and css -->
    <script type="text/javascript" src="<?php echo RFP_PLUGIN_URL; ?>/js/datepicker.js"></script>
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo RFP_PLUGIN_URL; ?>/css/base.css" />
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo RFP_PLUGIN_URL; ?>/css/clean.css" />
    
    <!-- Google Code Prettify -->
    <link href="<?php echo RFP_PLUGIN_URL; ?>/css/prettify.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo RFP_PLUGIN_URL; ?>/js/prettify.js"></script>
    
    <style type="text/css">
      /* Style the calendar custom widget */
      #date-range {
        position:relative;
      }
      #date-range-field {
        width: 235px;
        height: 26px;
        overflow: hidden;
        position: relative;
        cursor:pointer;
        border: 1px solid #CCCCCC;
        border-radius: 5px 5px 5px 5px;
		float:left;
      }
      #date-range-field a  {
        color:#B2B2B2;
        background-color:#F7F7F7;
        text-align:center;
        display: block;
        position: absolute;
        width: 26px;
        height: 23px;
        top: 0;
        right: 0;
        text-decoration: none;
        padding-top:6px;
        border-radius: 0 5px 5px 0;
      }
      #date-range-field span {
        font-size: 12px;
        font-weight: bold;
        color: #404040;
        position: relative;
        top: 0;
        height: 26px;
        line-height: 26px;
        left: 5px;
        width: 250px;
        text-align: center;
      }
      
      #datepicker-calendar {
        position: absolute;
        top: 27px;
        left: 0;
        overflow: hidden;
        width: 497px;
        height:153px;
        background-color: #F7F7F7;
        border: 1px solid #CCCCCC;
        border-radius: 0 5px 5px 5px;
        display:none;
        padding:10px 0 0 10px;
      }
      
      /* Remove default border from the custom widget since we're adding our own.  TBD: rework the dropdown calendar to use the default borders */
      #datepicker-calendar div.datepicker {
        background-color: transparent;
      border: none;
      border-radius: 0;
      padding: 0;
    }
    </style>
      
    <script type="text/javascript">
      
      $(document).ready(function() {
        
        // pretty-print the source
        prettyPrint();
        
        /* simple inline calendar */
        $('#simple-calendar').DatePicker({
          mode: 'single',
          inline: true,
          date: new Date()
        });
        
        /* multi inline calendar */
        $('#multi-calendar').DatePicker({
          mode: 'multiple',
          inline: true,
          calendars: 3,
          date: [new Date(), new Date() - 1000 * 60 * 60 * 24 * 2, new Date() - 1000 * 60 * 60 * 24 * 4]
        });
        
        /* Calendar tied to text input */
        $('#inputDate').DatePicker({
          mode: 'single',
          position: 'right',
          onBeforeShow: function(el){
            if($('#inputDate').val()) $('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
          },
          onChange: function(date, el) {
            $(el).val((date.getMonth()+1)+'/'+date.getDate()+'/'+date.getFullYear());
            if($('#closeOnSelect input').attr('checked')) {
              $(el).DatePickerHide();
            }
          }
        });
        
        /* Special date widget */
       
        var to = new Date();
        var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);
        
        $('#datepicker-calendar').DatePicker({
          inline: true,
          date: [from, to],
          calendars: 3,
          mode: 'range',
          current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
          onChange: function(dates,el) {
            // update the range display
            $('#date-range-field span').text(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
                                        dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
										
			document.getElementById('display_date').value=dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
                                        dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear();
          }
        });
        
        // initialize the special date dropdown field
        $('#date-range-field span').text(from.getDate()+' '+from.getMonthName(true)+', '+from.getFullYear()+' - '+
                                        to.getDate()+' '+to.getMonthName(true)+', '+to.getFullYear());
        
        // bind a click handler to the date display field, which when clicked
        // toggles the date picker calendar, flips the up/down indicator arrow,
        // and keeps the borders looking pretty
        $('#date-range-field').bind('click', function(){
          $('#datepicker-calendar').toggle();
          if($('#date-range-field a').text().charCodeAt(0) == 9660) {
            // switch to up-arrow
            $('#date-range-field a').html('&#9650;');
            $('#date-range-field').css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
            $('#date-range-field a').css({borderBottomRightRadius:0});
          } else {
            // switch to down-arrow
            $('#date-range-field a').html('&#9660;');
            $('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
            $('#date-range-field a').css({borderBottomRightRadius:5});
          }
          return false;
        });
        
        // global click handler to hide the widget calendar when it's open, and
        // some other part of the document is clicked.  Note that this works best
        // defined out here rather than built in to the datepicker core because this
        // particular example is actually an 'inline' datepicker which is displayed
        // by an external event, unlike a non-inline datepicker which is automatically
        // displayed/hidden by clicks within/without the datepicker element and datepicker respectively
        $('html').click(function() {
          if($('#datepicker-calendar').is(":visible")) {
            $('#datepicker-calendar').hide();
            $('#date-range-field a').html('&#9660;');
            $('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
            $('#date-range-field a').css({borderBottomRightRadius:5});
          }
        });
        
        // stop the click propagation when clicking on the calendar element
        // so that we don't close it
        $('#datepicker-calendar').click(function(event){
          event.stopPropagation();
        });
      });
      /* End special page widget */
    </script>
	<a href="admin.php?page=my-menu" style="float:right;"><?php echo htmlentities('More Info �'); ?></a>
 <form name="frm_content" id="frm_content" method="post" enctype="multipart/form-data" action="">
 <div class="alignleft actions">
			<select name="action">
<option selected="selected" value="-1">Bulk Actions</option>
	<option  value="edit">Edit</option>
	<option value="trash">Move to Trash</option>
</select>
<input type="submit" value="Apply" class="button-secondary action" id="doaction" name="">
		</div>
	
	<table class="rfptask-table widefat" id="rfptask-list" >
		<thead>
			<tr>
				<?php
					if ($advanced['show_id']==1) {
				?>
				<th id="id-col" class="header">ID</th>
				<?php
				}
				?>
				
				<th id="item-col" class="header ">Task</th>
				<?php
				
				
				 if ($options['category']==1) { ?>
				<th id="category-col" class="header">Category</th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th id="category-col" class="header">Time Frame</th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th id="category-col" class="header">Assigned To</th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th id="category-col" class="header">Added By</th>
				<?php
				}
				if ($priority==1) {
				?>
                <th id="category-col" class="header">Priority</th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Status</th>
				<?php
				}
				if ($duedate==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Due Date</th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Date Added</th>
				<?php
				}
				?>
			<th id="action-col" class="{sorter: false} no-sort">Actions</th>
				<th><!--<input type="checkbox" name="all" id="all"  value="all" onClick="javascript: check();" />--><!--<input type="image" name="btn" style=" border: 1px solid #B2B2B2; !important;border-radius: 6px 6px 6px 6px; !important;margin: 0 0 0 8px; !important; padding: 2px; !important;vertical-align: text-top; !important;" src="<?php echo RFP_PLUGIN_URL; ?>/images/delete_but.png">--></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
			<?php
				if ($advanced['show_id']==1) {
			?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				<?php if ($options['category']==1) { ?>
				<th></th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th></th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th></th>
				<?php
				}
				if ($priority==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th></th>
				<?php
				}
				if ($duedate==1) {
				?>
					<th></th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th></th>
				<?php
				}
				?>
				
				<th></th>
				
				<th></th>
				
			</tr>
		</tfoot>
		<tbody>
		


<?php
global $post;
//print_r($advanced);
$c_userid = get_current_user_id( );
if ($c_userid!=1) {
	if ($advanced['show_only_assigned']==1 && $advanced['show_only_assigned']!='') {
		
	$args = array(
			   'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'id',
			   'order'    => 'ASC',
			   'post_status' => 'publish',
			   'author'   => $c_userid
				); 
	} else {
	$args = array(
			  'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'post_status' => 'publish',
			   'order'    => 'ASC',
				  ); 
	}
} else { 
$args = array(
			  'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'post_status' => 'publish',
			   'order'    => 'ASC',
				  ); 
} 
		
			
$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();

//print_r($rfpquery->the_post());

?>
<tr>
<!-- RFP-Task Details Listing Page -->

<!-- ID of the task  --> 
<?php 
	if ($advanced['show_id']==1) {
?>
<td><?php echo get_the_ID(); ?> </td>
<?php
}
?>
<!--End ID-->


<!-- RFP Task List Title  --> 
                
<td><?php  the_title(); ?></td>

<!-- End RFP Task List --> 


<!-- RFP Task List Category  --> 
<?php
if ($options['category']==1) {
?>
<td>
<?php
//echo $post->ID;
  $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 //print_r($term_list);
  if ($term_list) {
   echo $term_list[0]->name;
}

?> 
</td>
<?php
			}
			if ($options['timeframe']==1) {
			?>		
<!-- End RFP Task List Category  --> 


<!-- RFP Task List TimeFrame  --> 
<td>
<!--<img src="<?php echo RFP_PLUGIN_URL; ?>/images/timer.png" />-->
<?php $meta2= get_the_ID(); 
	$time	=	get_post_meta($meta2, '_TimeFrame' , true);
?>
<?php if (!empty($time)) { echo get_post_meta($meta2, '_TimeFrame' , true);} else { echo "00"; } ?>&nbsp;Min
</td>
<!-- End RFP Task List TimeFrame  -->
 <?php
 }
 if ($assigned==1) {
 ?>
 
<!-- RFP Task List Assigned  -->
<td class=""><?php echo get_post_meta($meta2, '_assign' , true);  ?></td>
<!-- End RFP Task List Assigned  -->

 <?php
   }
   if ($options['assignedto']==1) {
   ?>
   <td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td>
   <?php
   }
   if ($priority==1) {
   ?>
<!-- RFP Task List Priority  -->
<?php $priority1	=	get_post_meta($meta2, '_priority' , true); ?>
<td><?php if ($priority1=='High') { ?><span style="color:#FF0000;"><?php echo $priority1; ?></span><?php } else { echo $priority1; } ?></td>
<!-- End RFP Task List Priority  -->
 <?php
   }
   if ($options['status']==1) {
   ?>

<!-- RFP Task List Status  -->
<td><?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></td>
<!-- End RFP Task List Status  -->
<?php
}
if ($duedate==1) {
?>
<td><?php echo date('M,d Y',get_post_meta($meta2, '_Date' , true));  ?></td>
<?php
}
?>
<!-- RFP Task List Action Section  -->
<?php
if ($advanced['show_date_added']==1) {



?>
   <td><?php $origpostdate = get_the_date($d, $the_post->post_parent);
               $origposttime = get_the_time($d, $the_post->post_parent);
               echo $dateline = $origpostdate; ?></td>
   <?php
   }
   ?>
<td><script>
$(document).ready(function(){

$('[rel=tooltip]').bind('mouseover', function(){
	  
		
	
 if ($(this).hasClass('ajax')) {
	var ajax = $(this).attr('ajax');	
			
  $.get(ajax,
  function(theMessage){
$('<div class="tooltip">'  + theMessage + '</div>').appendTo('body').fadeIn('fast');});

 
 }else{
			
	    var theMessage = $(this).attr('content');
	    $('<div class="tooltip">' + theMessage + '</div>').appendTo('body').fadeIn('fast');
		}
		
		$(this).bind('mousemove', function(e){
			$('div.tooltip').css({
				'top': e.pageY - ($('div.tooltip').height() / 2) - 5,
				'left': e.pageX + 15
			});
		});
	}).bind('mouseout', function(){
		$('div.tooltip').fadeOut('fast', function(){
			$(this).remove();
		});
	});
						   });

</script>
<style>
.tooltip{
	position:absolute;
	width:320px;
	background-image:url(<?php echo RFP_PLUGIN_URL; ?>/images/tip-bg.png);
	background-position:left center;
	background-repeat:no-repeat;
	color:#FFF;
	padding:5px 5px 5px 18px;
	font-size:12px;
	font-family:Verdana, Geneva, sans-serif;
	}
	
.tooltip-image{
	float:left;
	margin-right:5px;
	margin-bottom:5px;
	margin-top:3px;}	
	
	
	.tooltip span{font-weight:700;
color:#ffea00;}




li{
	margin-bottom:30px;}
	#imagcon{
		overflow:hidden;
		float:left;
		height:100px;
		width:100px;
		margin-right:5px;
	}
	#imagcon img{
		max-width:100px;
	}
	#wrapper{
		margin:0 auto;
		width:100px;
		/*margin-top: 99px;*/
	}
	.gallery > li {
    float: left;
    margin-bottom: -2px !important;
    margin-right: 5px;
}
.fleft1 {
overflow: hidden;position: relative;top: -12px;
}
</style>
			<div class="fleft1">
			
				<ul class="gallery clearfix">
				 	<li><a href="javascript: void(0)" onclick="javascript: RFP_Edit('<?php echo get_the_ID(); ?>')" title="Edit TaskList"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit.png" /></a></li>
					<li><a href="javascript: void(0)" onclick="javascript: RFP_Notes('<?php echo get_the_ID(); ?>')" title="Edit Notes"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit_icon.png" /></a></li>
					<li><a href="<?php echo get_post_meta($meta2, '_urladdress' , true);  ?>" rel="prettyPhoto" title="Video" ><img src="<?php echo RFP_PLUGIN_URL; ?>/images/video.png" /></a></li>
					<li><a href=# alt=Image Tooltip rel=tooltip content="<div id=con>Task Items:Task 1</div><div id=con>Category:SEO</div><div id=con>AddedBY:Admin</div><div id=con>AssignedTo:Rajeshkumar</div>"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/star.png" /></a></li>
				</ul>
			</div>

<!--&nbsp;&nbsp;<a class="star" style="bottom: 35px !important;left: 90px;position: relative;" href="javascript: void(0)"></a>&nbsp;&nbsp;-->

</td>

<!-- End RFP Task List Action Section  -->
<td>
<input type="hidden" name="CheckValue" id="CheckValue" value="Check" /><input type="checkbox" name="checkbox[]" id="checkbox[]"  value="<?php echo get_the_ID(); ?>"/>
</td>


</tr>
 
<?php
endwhile;
wp_reset_query();
?>	
<!--End Loop and Query -->
</tbody>
</table>
	
<!-- End Task listing Table -->
</form>
<p style="clear: both; text-align: right"><a href="admin.php?page=Add-Task-List"><?php $str	=	"Add Task  �"; echo htmlentities($str); ?></a></p>
	</div>
	<form name="RFP_Name" id="frm1" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="txt_rfpid" id="txt_rfpid" value="" />
			</form>
			
			<script language="javascript1.5" type="text/javascript">
			function RFP_Edit(id) {
			//alert(id);
			document.getElementById("txt_rfpid").value=id;
			
			document.getElementById("frm1").submit();
			//alert(id);
			}
			</script>
			<form name="RFP_Notes1" id="RFP_Notes1" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="txt_notes" id="txt_notes" value="" />
			</form>
			<script language="javascript1.5" type="text/javascript">
			function RFP_Notes(id) {
			//alert(id);
			document.getElementById("txt_notes").value=id;
			document.getElementById("RFP_Notes1").submit();
			}
			</script>
	
	<?php
	
	}
	
	public static function the_title1($before = '', $after = '', $echo = true) {
	$title = get_the_title();
	if ( strlen($title) == 0 )
		return;
	$title = $before . $title . $after;
	if ( $echo )
		echo wordwrap($title,  25, "<br/>", false);
	else
		return wordwrap($title,  25, "<br/>", false);
}
public static function get_users( $role ) {
		$wp_user_search = new WP_User_Query( array( 'role' => $role ) );
		return $wp_user_search->get_results();
	}
	public static function bulkactions() {
	//print_r($_REQUEST);
	
	   $options	 = get_option( 'rfplist_general' );
	   $advanced = get_option( 'rfplist_advanced' );
	 // print_r($advanced);
	global $wpdb;
	$options1 = $wpdb->get_results("SELECT * FROM wp_rfpscreenoptions WHERE id = 1");
	foreach($options1 as $screen)  {
	//print_r($screen);
	$priority	=	 $screen->priority;
	$duedate	=	 $screen->duedate;
	$assigned	=	 $screen->assigned;
	
	}
	?>
			<div class="wrap">
	
	<div class="icon32"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/quote_ic.png" width="32" height="32" /></div>
	<h2>Task List <a class="add-new-h2" href="<?php echo admin_url(); ?>admin.php?page=Add-Task-List">Add New</a> </h2>
    <h3>Task List</h3>
  
<form name="search" id="search" action="" method="post" enctype="multipart/form-data">
<p class="search-box">
	<label for="post-search-input" class="screen-reader-text">Search Pages:</label>
	<input type="text" value="" name="search" id="post-search-input">
	<input type="submit" value="Search Task List" class="button" id="search-submit" name="submit"></p>
	</form>
	<form method="post" action="" id="posts-filter">
	<div>
	

		
		<div class="alignleft actions">
		<?php
				$taxonomies	=	"rfpcat";	
				$args		=	array('orderby'=> 'term_id', 'order'=> 'DESC', 'hide_empty' => false); 
				$category 	=	get_terms( $taxonomies, $args ); //Get all categories
		?>
		<select name="Category">
		<option value="">View All Category</option>
		<?php 
		foreach($category as $cat) {
		?>
			<option value="<?php echo $cat->term_id; ?>" ><?php echo esc_attr($cat->name); ?></option>
			<?php 
			}
			?>
			
		</select>
		

<input type="submit" value="Filter" class="button-secondary" id="post-query-submit" name="filter">		</div>

</form>
<!--<div class="row">
        
        <div class="span8">
          <div id="date-range">
            <div id="date-range-field">
              <span></span>
              <a href="#">&#9660;</a>
            </div>
            <div id="datepicker-calendar"></div>
          </div>
        </div>
		 
	 
      </div>-->
	  <!--<div class="datepick">
	    <form name="frm1" method="post" action="" enctype="multipart/form-data">
    <input type="hidden" id="display_date" name="datepick" value="" />
      <input type="submit" name="submit" value="Filter" class="button-secondary" />
	  </form>
	  </div>-->
		<form name="frm_content" id="frm_content" method="post" enctype="multipart/form-data" action="">
		
	<div class="alignleft actions">
			<select name="action">
<option selected="selected" value="-1">Bulk Actions</option>
	<option  value="edit">Edit</option>
	<option value="trash">Move to Trash</option>
</select>
<input type="submit" value="Apply" class="button-secondary action" id="doaction" name="">
		</div>
	<table class="rfptask-table widefat" id="rfptask-list" >
		<thead>
			<tr>
				<?php
					if ($advanced['show_id']==1) {
				?>
				<th id="id-col" class="header">ID</th>
				<?php
				}
				?>
				
				<th id="item-col" class="header ">Task</th>
				<?php
				
				
				 if ($options['category']==1) { ?>
				<th id="category-col" class="header">Category</th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th id="category-col" class="header">Time Frame</th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th id="category-col" class="header">Assigned To</th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th id="category-col" class="header">Added By</th>
				<?php
				}
				if ($priority==1) {
				?>
                <th id="category-col" class="header">Priority</th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Status</th>
				<?php
				}
				if ($duedate==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Due Date</th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Date Added</th>
				<?php
				}
				?>
				<th id="action-col" class="{sorter: false} no-sort">Actions</th>
				<th><!--<input type="checkbox" name="all" id="all"  value="all" onClick="javascript: check();" />--><!--<input type="image" name="btn" style=" border: 1px solid #B2B2B2; !important;border-radius: 6px 6px 6px 6px; !important;margin: 0 0 0 8px; !important; padding: 2px; !important;vertical-align: text-top; !important;" src="<?php echo RFP_PLUGIN_URL; ?>/images/delete_but.png">--></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
			<?php
				if ($advanced['show_id']==1) {
			?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				<?php if ($options['category']==1) { ?>
				<th></th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th></th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th></th>
				<?php
				}
				if ($priority==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th></th>
				<?php
				}
				if ($duedate==1) {
				?>
					<th></th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				
				<th></th>
				
			</tr>
		</tfoot>
		<tbody>
				
<tr style="display: table-row;" class="inline-edit-row inline-edit-row-post inline-edit-post bulk-edit-row bulk-edit-row-post bulk-edit-post inline-editor" id="bulk-edit"><td class="colspanchange" colspan="9">

		<fieldset class="inline-edit-col-left"><div class="inline-edit-col">
			<h4>Bulk Edit</h4>
				<div id="bulk-title-div">
				<div id="bulk-titles">
				<?php
				if (isset($_REQUEST['checkbox'])) {
				foreach($_REQUEST['checkbox'] as $edit) {
				
				$editactions = $wpdb->get_results("SELECT * FROM wp_posts WHERE ID = '$edit'");
				 //print_r($editactions);
				 foreach($editactions as $bulk) {  
				?>
				<div id="ttle126"><input type="checkbox" name="task_name[]" value="<?php echo $bulk->ID; ?>"/> <?php echo $bulk->post_title; ?></div>
				
				<?php
					}
				}
			} else {
			?>
			<script type="text/javascript" language="javascript1.5">
			alert('Please select atleast one task Items!');
			window.location='<?php echo admin_url(); ?>admin.php?page=my-menu';
			</script>
			<?php
			}
				?>
				</div>
			</div>

	
	
	
		</div></fieldset>
		
		<div class="inline-edit-group">
		<label class="alignleft">
				<span class="title">Status</span>
				<select name="Status" id="Status" class="regular-text">
						<option value="Not Started" >Not Started</option>
						<option value="In Progress">In Progress</option>
						<option value="Completed" >Completed</option>
						<option value="Irrelevant">Irrelevant</option>
						<option value="Deferred" >Deferred</option>
					</select>
			</label>
					<label class="alignleft">
				<span class="title">Categories</span>
				<?php
				$taxonomies	=	"rfpcat";	
				$args		=	array('orderby'=> 'term_id', 'order'=> 'DESC', 'hide_empty' => false); 
				$category 	=	get_terms( $taxonomies, $args ); //Get all categories
		?>
		<select name="Category">
		<option value="">View All Category</option>
		<?php 
		foreach($category as $cat) {
		?>
			<option value="<?php echo $cat->term_id; ?>" ><?php echo esc_attr($cat->name); ?></option>
			<?php 
			}
			?>
			
		</select>
			</label>
			<?php
			$advanced = get_option( 'rfplist_advanced' );
			//echo $advanced['user_roles'][1];
			include "user.inc.php";
			
			//print_r($roles);
			
			?>
					<label class="alignright">
				<span class="title">AssignedTo</span>
				
						<select name="AssignedTo" id="AssignedTo">
						<option value="">--SELECT ONE--</option>
					<?php
					
					
					foreach ( $roles as $role ) {
					$role_users = self::get_users( $role );
					
					//print_r($role_users);
					
					
						foreach ( $role_users as $role_user ) {
						
						?>
						<option value="<?php echo $role_user->ID; ?>"<?php if (isset($id)) if ($assign==$role_user->display_name) { echo "selected=selected"; } ?>><?php echo esc_attr($role_user->display_name); ?></option>
						<?php
								//echo $role_user->display_name."<br/>";
						}
					}
					
?>
				<select>
			</label>
			
					</div>
	

			<p class="submit inline-edit-save">
		
			<input type="submit" value="Update" class="button-primary alignright" id="bulk_edit" name="bulk_edit">
			<span style="display:none" class="error"></span>
			<br class="clear">
		</p>
		</td><td></td>
		</tr>
<!--End Loop and Query -->
</tbody>
</table>
<!-- End Task listing Table -->
</form>
</div>

	<?php

	}
public static function tasklisttrash() {
if (isset($_REQUEST['checkbox']) && $_REQUEST['checkbox']!='') {
		foreach ($_REQUEST['checkbox'] as $id) {
			$my_post = array(
				'ID'			   => $id,
				'post_type'        => 'rfptask',
				'post_status'      => 'trash',
				);
		 $post_id 	 =  wp_update_post( $my_post );
		
		 }
		
		 ?>
		 <script type="text/javascript" language="javascript1.5">
		 alert("Selected items move to trash successfully!");
		window.location='<?php echo admin_url(); ?>admin.php?page=my-menu';
		 </script>
		 <?php
			
	} else {
	 $redirect	=	admin_url()."admin.php?page=my-menu";
	wp_redirect($redirect);exit;
	}
}
public static function taskliststatus() {

  //echo TEMPLATEPATH;
	   $options	 = get_option( 'rfplist_general' );
	   $advanced = get_option( 'rfplist_advanced' );
	  // print_r($advanced);
	global $wpdb;
	$options1 = $wpdb->get_results("SELECT * FROM wp_rfpscreenoptions WHERE id = 1");
	foreach($options1 as $screen)  {
	//print_r($screen);
	$priority	=	 $screen->priority;
	$duedate	=	 $screen->duedate;
	$assigned	=	 $screen->assigned;

	}
	?>

	<div class="wrap">
	
	<div class="icon32"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/quote_ic.png" width="32" height="32" /></div>
	<h2>Task List <a class="add-new-h2" href="<?php echo admin_url(); ?>admin.php?page=Add-Task-List">Add New</a> </h2></strong>
    <h3>Task List</h3>
  <script language="javascript1.5" type="text/javascript">

function checkedAll ()
{
var aa= document.getElementById('frm_content');
for (var i =0; i < aa.elements.length; i++)
{
aa.elements[i].checked = true;
}
}
function uncheckedAll ()
{
var aa= document.getElementById('frm_content');
for (var i =0; i < aa.elements.length; i++)
{
aa.elements[i].checked = false;
}
}
function check()
{
	//alert(document.getElementById('all').checked)
	if(document.getElementById('all').checked)
	{
		checkedAll ()
		
	}
	else
	{
		uncheckedAll ()
	}
}

</script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
		 <link rel="stylesheet" href="<?php echo RFP_PLUGIN_URL; ?>/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
		<script src="<?php echo RFP_PLUGIN_URL; ?>/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	 <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$("area[rel^='prettyPhoto']").prettyPhoto();
				
				$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: true});
				$(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
		
				$("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
					custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
					changepicturecallback: function(){ initialize(); }
				});

				$("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
					custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
					changepicturecallback: function(){ _bsap.exec(); }
				});
			});
			</script>
	   <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
     <!-- datepicker javascript and css -->
    <script type="text/javascript" src="<?php echo RFP_PLUGIN_URL; ?>/js/datepicker.js"></script>
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo RFP_PLUGIN_URL; ?>/css/base.css" />
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo RFP_PLUGIN_URL; ?>/css/clean.css" />
    
    <!-- Google Code Prettify -->
    <link href="<?php echo RFP_PLUGIN_URL; ?>/css/prettify.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo RFP_PLUGIN_URL; ?>/js/prettify.js"></script>
    
    <style type="text/css">
      /* Style the calendar custom widget */
      #date-range {
        position:relative;
      }
      #date-range-field {
        width: 235px;
        height: 26px;
        overflow: hidden;
        position: relative;
        cursor:pointer;
        border: 1px solid #CCCCCC;
        border-radius: 5px 5px 5px 5px;
		float:left;
      }
      #date-range-field a  {
        color:#B2B2B2;
        background-color:#F7F7F7;
        text-align:center;
        display: block;
        position: absolute;
        width: 26px;
        height: 23px;
        top: 0;
        right: 0;
        text-decoration: none;
        padding-top:6px;
        border-radius: 0 5px 5px 0;
      }
      #date-range-field span {
        font-size: 12px;
        font-weight: bold;
        color: #404040;
        position: relative;
        top: 0;
        height: 26px;
        line-height: 26px;
        left: 5px;
        width: 250px;
        text-align: center;
      }
      
      #datepicker-calendar {
        position: absolute;
        top: 27px;
        left: 0;
        overflow: hidden;
        width: 497px;
        height:153px;
        background-color: #F7F7F7;
        border: 1px solid #CCCCCC;
        border-radius: 0 5px 5px 5px;
        display:none;
        padding:10px 0 0 10px;
      }
      
      /* Remove default border from the custom widget since we're adding our own.  TBD: rework the dropdown calendar to use the default borders */
      #datepicker-calendar div.datepicker {
        background-color: transparent;
      border: none;
      border-radius: 0;
      padding: 0;
    }
    </style>
      
    <script type="text/javascript">
      
      $(document).ready(function() {
        
        // pretty-print the source
        prettyPrint();
        
        /* simple inline calendar */
        $('#simple-calendar').DatePicker({
          mode: 'single',
          inline: true,
          date: new Date()
        });
        
        /* multi inline calendar */
        $('#multi-calendar').DatePicker({
          mode: 'multiple',
          inline: true,
          calendars: 3,
          date: [new Date(), new Date() - 1000 * 60 * 60 * 24 * 2, new Date() - 1000 * 60 * 60 * 24 * 4]
        });
        
        /* Calendar tied to text input */
        $('#inputDate').DatePicker({
          mode: 'single',
          position: 'right',
          onBeforeShow: function(el){
            if($('#inputDate').val()) $('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
          },
          onChange: function(date, el) {
            $(el).val((date.getMonth()+1)+'/'+date.getDate()+'/'+date.getFullYear());
            if($('#closeOnSelect input').attr('checked')) {
              $(el).DatePickerHide();
            }
          }
        });
        
        /* Special date widget */
       
        var to = new Date();
        var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);
        
        $('#datepicker-calendar').DatePicker({
          inline: true,
          date: [from, to],
          calendars: 3,
          mode: 'range',
          current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
          onChange: function(dates,el) {
            // update the range display
            $('#date-range-field span').text(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
                                        dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
										
			document.getElementById('display_date').value=dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
                                        dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear();
          }
        });
        
        // initialize the special date dropdown field
        $('#date-range-field span').text(from.getDate()+' '+from.getMonthName(true)+', '+from.getFullYear()+' - '+
                                        to.getDate()+' '+to.getMonthName(true)+', '+to.getFullYear());
        
        // bind a click handler to the date display field, which when clicked
        // toggles the date picker calendar, flips the up/down indicator arrow,
        // and keeps the borders looking pretty
        $('#date-range-field').bind('click', function(){
          $('#datepicker-calendar').toggle();
          if($('#date-range-field a').text().charCodeAt(0) == 9660) {
            // switch to up-arrow
            $('#date-range-field a').html('&#9650;');
            $('#date-range-field').css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
            $('#date-range-field a').css({borderBottomRightRadius:0});
          } else {
            // switch to down-arrow
            $('#date-range-field a').html('&#9660;');
            $('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
            $('#date-range-field a').css({borderBottomRightRadius:5});
          }
          return false;
        });
        
        // global click handler to hide the widget calendar when it's open, and
        // some other part of the document is clicked.  Note that this works best
        // defined out here rather than built in to the datepicker core because this
        // particular example is actually an 'inline' datepicker which is displayed
        // by an external event, unlike a non-inline datepicker which is automatically
        // displayed/hidden by clicks within/without the datepicker element and datepicker respectively
        $('html').click(function() {
          if($('#datepicker-calendar').is(":visible")) {
            $('#datepicker-calendar').hide();
            $('#date-range-field a').html('&#9660;');
            $('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
            $('#date-range-field a').css({borderBottomRightRadius:5});
          }
        });
        
        // stop the click propagation when clicking on the calendar element
        // so that we don't close it
        $('#datepicker-calendar').click(function(event){
          event.stopPropagation();
        });
      });
      /* End special page widget */
    </script>
<form name="search" id="search" action="" method="post" enctype="multipart/form-data">
<p class="search-box">
	<label for="post-search-input" class="screen-reader-text">Search Pages:</label>
	<input type="text" value="" name="search" id="post-search-input">
	<input type="submit" value="Search Task List" class="button" id="search-submit" name="submit"></p>
	</form>
	<form method="post" action="" id="posts-filter">
	<div>
	

	
		<div class="alignleft actions" >
		<?php
				$taxonomies	=	"rfpcat";	
				$args		=	array('orderby'=> 'term_id', 'order'=> 'DESC', 'hide_empty' => false); 
				$category 	=	get_terms( $taxonomies, $args ); //Get all categories
		?>
		<select name="Category">
		<option value="">View All Category</option>
		<?php 
		foreach($category as $cat) {
		?>
			<option value="<?php echo $cat->term_id; ?>" ><?php echo esc_attr($cat->name); ?></option>
			<?php 
			}
			?>
			
		</select>
<input type="submit" value="Filter" class="button-secondary" id="post-query-submit" name="filter">		</div>

		


</form>
	<form method="post" action="" id="posts-filter" enctype="multipart/form-data">
 <div class="alignleft actions">
	
		<select name="status">
		<option value="">All</option>
		<option value="Completed" <?php if (isset($_REQUEST['status'])) if ($_REQUEST['status']=='Completed') { echo "selected='selected'"; } ?> >Completed</option>
		<option value="In Progress" <?php if (isset($_REQUEST['status'])) if ($_REQUEST['status']=='In Progress') { echo "selected='selected'"; } ?> >In Progress</option>
		<option value="Deferred" <?php if (isset($_REQUEST['status'])) if ($_REQUEST['status']=='Deferred') { echo "selected='selected'"; } ?> >Deferred</option>	
		</select>
		
		</div>
		<input type="submit" value="Filter" class="button-secondary" id="post-query-submit" name="filter1">
		</form>

	 <form name="frm_content" id="frm_content" method="post" enctype="multipart/form-data" action="">
	
	<table class="rfptask-table widefat" id="rfptask-list" >
		<thead>
			<tr>
				<?php
					if ($advanced['show_id']==1) {
				?>
				<th id="id-col" class="header">ID</th>
				<?php
				}
				?>
				
				<th id="item-col" class="header ">Task</th>
				<?php
				
				
				 if ($options['category']==1) { ?>
				<th id="category-col" class="header">Category</th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th id="category-col" class="header">Time Frame</th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th id="category-col" class="header">Assigned To</th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th id="category-col" class="header">Added By</th>
				<?php
				}
				if ($priority==1) {
				?>
                <th id="category-col" class="header">Priority</th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Status</th>
				<?php
				}
				if ($duedate==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Due Date</th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Date Added</th>
				<?php
				}
				?>
				<th id="action-col" class="{sorter: false} no-sort">Actions</th>
				<th class="{sorter: false} no-sort"><!--<input type="checkbox" name="all" id="all"  value="all" onClick="javascript: check();" />--><!--<input type="image" name="btn" style=" border: 1px solid #B2B2B2; !important;border-radius: 6px 6px 6px 6px; !important;margin: 0 0 0 8px; !important; padding: 2px; !important;vertical-align: text-top; !important;" src="<?php echo RFP_PLUGIN_URL; ?>/images/delete_but.png">--></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
			<?php
				if ($advanced['show_id']==1) {
			?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				<?php if ($options['category']==1) { ?>
				<th></th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th></th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['assignedto']==1) {

				?>
				<th></th>
				<?php
				}
				if ($priority==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th></th>
				<?php
				}
				if ($duedate==1) {
				?>
					<th></th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				
				<th></th>
			</tr>
		</tfoot>
		<tbody>
	<?php
    // Query arguments
	
	$args = array(
			
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => $_REQUEST['status'])),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'post_status' => 'publish',
			   'post__in' => $post_ids,
			   'orderby'  => 'ID',
			   'order'    => 'ASC'			  
				  ); 
	
   // $args = array('post_type' => 'rfptask','post_status' => 'publish','post__in' => $post_ids);
    $query = new WP_Query( $args );
    if ( $query->have_posts() ): while ( $query->have_posts() ) : $query->the_post();
    // Do loop here

?>

<tr>
<!-- RFP-Task Details Listing Page -->

<!-- ID of the task  --> 
<?php 
	if ($advanced['show_id']==1) {
?>
<td><?php echo get_the_ID(); ?> </td>
<?php
}
?>
<!--End ID-->


<!-- RFP Task List Title  --> 
                
<td><?php  self::the_title1(); ?></td>

<!-- End RFP Task List --> 


<!-- RFP Task List Category  --> 
<?php
if ($options['category']==1) {
?>
<td>
<?php
//echo $post->ID;
  $term_list = wp_get_post_terms(get_the_ID(), 'rfpcat', array("fields" => "all"));
 //print_r($term_list);
  if ($term_list) {
   echo $term_list[0]->name;
}

?> 
</td>
<?php
			}
			if ($options['timeframe']==1) {
			?>		
<!-- End RFP Task List Category  --> 


<!-- RFP Task List TimeFrame  --> 
<td>
<!--<img src="<?php echo RFP_PLUGIN_URL; ?>/images/timer.png" />-->
<?php $meta2= get_the_ID(); 
	$time	=	get_post_meta($meta2, '_TimeFrame' , true);
?>
<?php if (!empty($time)) { echo get_post_meta($meta2, '_TimeFrame' , true);} else { echo "00"; } ?>&nbsp;Min
</td>
<!-- End RFP Task List TimeFrame  -->
 <?php
 }
 if ($assigned==1) {
 ?>
 
<!-- RFP Task List Assigned  -->
<td class=""><?php echo get_post_meta($meta2, '_assign' , true);  ?></td>
<!-- End RFP Task List Assigned  -->

 <?php
   }
   if ($options['assignedto']==1) {
   ?>
   <td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td>
   <?php
   }
   if ($priority==1) {
   ?>
<!-- RFP Task List Priority  -->
<?php $priority1=	get_post_meta($meta2, '_priority' , true); ?>
<td><?php if ($priority1=='High') { ?><span style="color:#FF0000;"><?php echo $priority1; ?></span><?php } else { echo $priority1; } ?></td>
<!-- End RFP Task List Priority  -->
 <?php
   }
   if ($options['status']==1) {
   ?>

<!-- RFP Task List Status  -->
<td><?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></td>
<!-- End RFP Task List Status  -->
<?php
}
if ($duedate==1) {
?>
<td><?php $date	=	strtotime(get_post_meta($meta2, '_Date' , true)); echo date('M d,Y',$date);  ?></td>
<?php
}
?>
<!-- RFP Task List Action Section  -->
<?php
if ($advanced['show_date_added']==1) {



?>
   <td><?php $origpostdate = get_the_date($d, $the_post->post_parent);
               $origposttime = get_the_time($d, $the_post->post_parent);
               echo $dateline = $origpostdate; ?></td>
   <?php
   }
   ?>
<td>
<script>
$(document).ready(function(){

$('[rel=tooltip]').bind('mouseover', function(){
	  
		
	
 if ($(this).hasClass('ajax')) {
	var ajax = $(this).attr('ajax');	
			
  $.get(ajax,
  function(theMessage){
$('<div class="tooltip">'  + theMessage + '</div>').appendTo('body').fadeIn('fast');});

 
 }else{
			
	    var theMessage = $(this).attr('content');
	    $('<div class="tooltip">' + theMessage + '</div>').appendTo('body').fadeIn('fast');
		}
		
		$(this).bind('mousemove', function(e){
			$('div.tooltip').css({
				'top': e.pageY - ($('div.tooltip').height() / 2) - 5,
				'left': e.pageX + 15
			});
		});
	}).bind('mouseout', function(){
		$('div.tooltip').fadeOut('fast', function(){
			$(this).remove();
		});
	});
						   });

</script>
<style>
.tooltip{
	position:absolute;
	width:320px;
	background-image:url(<?php echo RFP_PLUGIN_URL; ?>/images/tip-bg.png);
	background-position:left center;
	background-repeat:no-repeat;
	color:#FFF;
	padding:5px 5px 5px 18px;
	font-size:12px;
	font-family:Verdana, Geneva, sans-serif;
	}
	
.tooltip-image{
	float:left;
	margin-right:5px;
	margin-bottom:5px;
	margin-top:3px;}	
	
	
	.tooltip span{font-weight:700;
color:#ffea00;}




li{
	margin-bottom:30px;}
	#imagcon{
		overflow:hidden;
		float:left;
		height:100px;
		width:100px;
		margin-right:5px;
	}
	#imagcon img{
		max-width:100px;
	}
	#wrapper{
		margin:0 auto;
		width:100px;
		/*margin-top: 99px;*/
	}
.gallery > li {
    float: left;
    margin-bottom: -2px !important;
    margin-right: 5px;
}
.fleft1 {
overflow: hidden;position: relative;top: -12px;
}
</style>
			<div class="fleft1" >
			
				<ul class="gallery clearfix">
				 	<li><a href="javascript: void(0)" onclick="javascript: RFP_Edit('<?php echo get_the_ID(); ?>')" title="Edit TaskList"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit.png" /></a></li>
					<li><a href="javascript: void(0)" onclick="javascript: RFP_Notes('<?php echo get_the_ID(); ?>')" title="Edit Notes"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit_icon.png" /></a></li>
					<li><a href="<?php echo get_post_meta($meta2, '_urladdress' , true);  ?>" rel="prettyPhoto" title="Video" ><img src="<?php echo RFP_PLUGIN_URL; ?>/images/video.png" /></a></li>
					<li><a href=# alt=Image Tooltip rel=tooltip content="<div id=con>Task Items:<?php  self::the_title1(); ?></div><div id=con>Category:<?php  if ($term_list) {  echo $term_list[0]->name; } ?></div><div id=con>Status:<?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></div><div id=con>AssignedTo:<?php echo get_post_meta($meta2, '_addedto' , true);  ?></div>"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/star.png" /></a></li>
				</ul>
			</div>
</td>

<!-- End RFP Task List Action Section  -->
<td>
<!--<input type="hidden" name="CheckValue" id="CheckValue" value="Check" /><input type="checkbox" name="checkbox[]" id="checkbox[]"  value="<?php echo get_the_ID(); ?>"/>-->
</td>
</tr>
<?php
	
    endwhile; endif;
	?>
			<!--End Loop and Query -->
</tbody>
</table>
	
<!-- End Task listing Table -->
</form>

	</div>
<!-- End Task listing Table -->
<form name="RFP_Name" id="frm1" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="txt_rfpid" id="txt_rfpid" value="" />
			</form>
			<script language="javascript1.5" type="text/javascript">
			function RFP_Edit(id) {
			//alert(id);
			document.getElementById("txt_rfpid").value=id;
			
			document.getElementById("frm1").submit();
			//alert(id);
			}
			</script>
			<form name="RFP_Notes1" id="RFP_Notes1" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="txt_notes" id="txt_notes" value="" />
			</form>
			<script language="javascript1.5" type="text/javascript">
			function RFP_Notes(id) {
			//alert(id);
			document.getElementById("txt_notes").value=id;
			document.getElementById("RFP_Notes1").submit();
			}
			</script>
	
	<?php
}
function bulkactions1() {

	//print_r($_REQUEST);
	
	   $options	 = get_option( 'rfplist_general' );
	   $advanced = get_option( 'rfplist_advanced' );
	 // print_r($advanced);
	global $wpdb;
	$options1 = $wpdb->get_results("SELECT * FROM wp_rfpscreenoptions WHERE id = 1");
	foreach($options1 as $screen)  {
	//print_r($screen);
	$priority	=	 $screen->priority;
	$duedate	=	 $screen->duedate;
	$assigned	=	 $screen->assigned;
	
	}
	?>
			<div class="wrap">
	


	

	  
		<form name="frm_content" id="frm_content" method="post" enctype="multipart/form-data" action="">
		
	
	<table class="rfptask-table widefat" id="rfptask-list" >
		<thead>
			<tr>
				<?php
					if ($advanced['show_id']==1) {
				?>
				<th id="id-col" class="header">ID</th>
				<?php
				}
				?>
				
				<th id="item-col" class="header ">Task</th>
				<?php
				
				
				 if ($options['category']==1) { ?>
				<th id="category-col" class="header">Category</th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th id="category-col" class="header">Time Frame</th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th id="category-col" class="header">Assigned To</th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th id="category-col" class="header">Added By</th>
				<?php
				}
				if ($priority==1) {
				?>
                <th id="category-col" class="header">Priority</th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Status</th>
				<?php
				}
				if ($duedate==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Due Date</th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th id="category-col" class="{sorter: false} no-sort">Date Added</th>
				<?php
				}
				?>
				<th id="action-col" class="{sorter: false} no-sort">Actions</th>
				<th class="{sorter: false} no-sort"><!--<input type="checkbox" name="all" id="all"  value="all" onClick="javascript: check();" />--><!--<input type="image" name="btn" style=" border: 1px solid #B2B2B2; !important;border-radius: 6px 6px 6px 6px; !important;margin: 0 0 0 8px; !important; padding: 2px; !important;vertical-align: text-top; !important;" src="<?php echo RFP_PLUGIN_URL; ?>/images/delete_but.png">--></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
			<?php
				if ($advanced['show_id']==1) {
			?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				<?php if ($options['category']==1) { ?>
				<th></th>
				<?php
				}
				if ($options['timeframe']==1) {
				?>
				<th></th>
				<?php
				}
				if ($assigned==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['assignedto']==1) {
				?>
				<th></th>
				<?php
				}
				if ($priority==1) {
				?>
				<th></th>
				<?php
				}
				if ($options['status']==1) {
				?>
				<th></th>
				<?php
				}
				if ($duedate==1) {
				?>
					<th></th>
				<?php
				}
				if ($advanced['show_date_added']==1) {
				?>
				<th></th>
				<?php
				}
				?>
				<th></th>
				
				<th></th>
				
			</tr>
		</tfoot>
		<tbody>
				
<tr style="display: table-row;" class="inline-edit-row inline-edit-row-post inline-edit-post bulk-edit-row bulk-edit-row-post bulk-edit-post inline-editor" id="bulk-edit"><td class="colspanchange" colspan="9">

		<fieldset class="inline-edit-col-left"><div class="inline-edit-col">
			<h4>Bulk Edit</h4>
				<div id="bulk-title-div">
				<div id="bulk-titles">
				<?php
				if (isset($_REQUEST['checkbox'])) {
				foreach($_REQUEST['checkbox'] as $edit) {
				
				$editactions = $wpdb->get_results("SELECT * FROM wp_posts WHERE ID = '$edit'");
				 //print_r($editactions);
				 foreach($editactions as $bulk) {  
				?>
				<div id="ttle126"><input type="checkbox" name="task_name[]" value="<?php echo $bulk->ID; ?>"/> <?php echo $bulk->post_title; ?></div>
				
				<?php
					}
				}
			} else {
			?>
			<script type="text/javascript" language="javascript1.5">
			alert('Please select atleast one task Items!');
			window.location='<?php echo admin_url(); ?>admin.php?page=my-menu';
			</script>
			<?php
			}
				?>
				</div>
			</div>

	
	
	
		</div></fieldset>
		
		<div class="inline-edit-group">
		<label class="alignleft">
				<span class="title">Status</span>
				<select name="Status" id="Status" class="regular-text">
						<option value="Not Started" >Not Started</option>
						<option value="In Progress">In Progress</option>
						<option value="Completed" >Completed</option>
						<option value="Irrelevant">Irrelevant</option>
						<option value="Deferred" >Deferred</option>
					</select>
			</label>
					<label class="alignleft">
				<span class="title">Categories</span>
				<?php
				$taxonomies	=	"rfpcat";	
				$args		=	array('orderby'=> 'term_id', 'order'=> 'DESC', 'hide_empty' => false); 
				$category 	=	get_terms( $taxonomies, $args ); //Get all categories
		?>
		<select name="Category">
		<option value="">View All Category</option>
		<?php 
		foreach($category as $cat) {
		?>
			<option value="<?php echo $cat->term_id; ?>" ><?php echo esc_attr($cat->name); ?></option>
			<?php 
			}
			?>
			
		</select>
			</label>
			<?php
			$advanced = get_option( 'rfplist_advanced' );
			//echo $advanced['user_roles'][1];
			include "user.inc.php";
			
			//print_r($roles);
			
			?>
					<label class="alignright">
				<span class="title">AssignedTo</span>
				
						<select name="AssignedTo" id="AssignedTo">
						<option value="">--SELECT ONE--</option>
					<?php
					
					
					foreach ( $roles as $role ) {
					$role_users = self::get_users( $role );
					
					//print_r($role_users);
					
					
						foreach ( $role_users as $role_user ) {
						
						?>
						<option value="<?php echo $role_user->ID; ?>"<?php if (isset($id)) if ($assign==$role_user->display_name) { echo "selected=selected"; } ?>><?php echo esc_attr($role_user->display_name); ?></option>
						<?php
								//echo $role_user->display_name."<br/>";
						}
					}
					
?>
				<select>
			</label>
			
					</div>
	

			<p class="submit inline-edit-save">
		
			<input type="submit" value="Update" class="button-primary alignright" id="bulk_edit" name="bulk_edit">
			<span style="display:none" class="error"></span>
			<br class="clear">
		</p>
		</td></tr>

<?php
global $post;
//print_r($advanced);
$c_userid = get_current_user_id( );
if ($c_userid!=1) {
	if ($advanced['show_only_assigned']==1 && $advanced['show_only_assigned']!='') {
		
	$args = array(
				'posts_per_page'  => $advanced['taskdisplay'],
			   'numberposts'     => $advanced['taskdisplay'],
			    'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'order'    => 'ASC',
			   'post_status' => 'publish',
			   'author'   => $c_userid
				); 
	} else {
	$args = array(
				'posts_per_page'  => $advanced['taskdisplay'],
			   'numberposts'     => $advanced['taskdisplay'],
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'post_status' => 'publish',
			   'order'    => 'ASC'
			  	  ); 
	}
} else { 
$args = array(
				'posts_per_page'  => $advanced['taskdisplay'],
			   'numberposts'     => $advanced['taskdisplay'],
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'post_status' => 'publish',
			   'order'    => 'ASC'			  
				  ); 
} 
	
			
$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();

//print_r($rfpquery->the_post());

?>
<tr>
<!-- RFP-Task Details Listing Page -->

<!-- ID of the task  --> 
<?php 
	if ($advanced['show_id']==1) {
?>
<td><?php echo get_the_ID(); ?> </td>
<?php
}
?>
<!--End ID-->


<!-- RFP Task List Title  --> 
                
<td><?php  self::the_title1();

 ?></td>

<!-- End RFP Task List --> 


<!-- RFP Task List Category  --> 
<?php
if ($options['category']==1) {
?>
<td>
<?php
//echo $post->ID;
  $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 //print_r($term_list);
  if ($term_list) {
   echo $term_list[0]->name;
}

?> 
</td>
<?php
			}
			if ($options['timeframe']==1) {
			?>		
<!-- End RFP Task List Category  --> 


<!-- RFP Task List TimeFrame  --> 
<td>
<!--<img src="<?php echo RFP_PLUGIN_URL; ?>/images/timer.png" />-->
<?php $meta2= get_the_ID(); 
	$time	=	get_post_meta($meta2, '_TimeFrame' , true);
?>
<?php if (!empty($time)) { echo get_post_meta($meta2, '_TimeFrame' , true);} else { echo "00"; } ?>&nbsp;Min
</td>
<!-- End RFP Task List TimeFrame  -->
 <?php
 }
 if ($assigned==1) {
 ?>
 
<!-- RFP Task List Assigned  -->
<td class=""><?php echo get_post_meta($meta2, '_assign' , true);  ?></td>
<!-- End RFP Task List Assigned  -->

 <?php
   }
   if ($options['assignedto']==1) {
   ?>
   <td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td>
   <?php
   }
   if ($priority==1) {
   ?>
<!-- RFP Task List Priority  -->
<?php $priority1	=	get_post_meta($meta2, '_priority' , true); ?>
<td><?php if ($priority1=='High') { ?><span style="color:#FF0000;"><?php echo $priority1; ?></span><?php } else { echo $priority1; } ?></td>
<!-- End RFP Task List Priority  -->
 <?php
   }
   if ($options['status']==1) {
   ?>

<!-- RFP Task List Status  -->
<td><?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></td>
<!-- End RFP Task List Status  -->
<?php
}
if ($duedate==1) {
?>
<td><?php echo get_post_meta($meta2, '_Date' , true);  ?></td>
<?php
}
?>
<!-- RFP Task List Action Section  -->
<?php
if ($advanced['show_date_added']==1) {



?>
   <td><?php $origpostdate = get_the_date($d, $the_post->post_parent);
               $origposttime = get_the_time($d, $the_post->post_parent);
               echo $dateline = $origpostdate; ?></td>
   <?php
   }
   ?>
<td>
	<script>
$(document).ready(function(){

$('[rel=tooltip]').bind('mouseover', function(){
	  
		
	
 if ($(this).hasClass('ajax')) {
	var ajax = $(this).attr('ajax');	
			
  $.get(ajax,
  function(theMessage){
$('<div class="tooltip">'  + theMessage + '</div>').appendTo('body').fadeIn('fast');});

 
 }else{
			
	    var theMessage = $(this).attr('content');
	    $('<div class="tooltip">' + theMessage + '</div>').appendTo('body').fadeIn('fast');
		}
		
		$(this).bind('mousemove', function(e){
			$('div.tooltip').css({
				'top': e.pageY - ($('div.tooltip').height() / 2) - 5,
				'left': e.pageX + 15
			});
		});
	}).bind('mouseout', function(){
		$('div.tooltip').fadeOut('fast', function(){
			$(this).remove();
		});
	});
						   });

</script>
<style>
.tooltip{
	position:absolute;
	width:320px;
	background-image:url(<?php echo RFP_PLUGIN_URL; ?>/images/tip-bg.png);
	background-position:left center;
	background-repeat:no-repeat;
	color:#FFF;
	padding:5px 5px 5px 18px;
	font-size:12px;
	font-family:Verdana, Geneva, sans-serif;
	}
	
.tooltip-image{
	float:left;
	margin-right:5px;
	margin-bottom:5px;
	margin-top:3px;}	
	
	
	.tooltip span{font-weight:700;
color:#ffea00;}




li{
	margin-bottom:30px;}
	#imagcon{
		overflow:hidden;
		float:left;
		height:100px;
		width:100px;
		margin-right:5px;
	}
	#imagcon img{
		max-width:100px;
	}
	#wrapper{
		margin:0 auto;
		width:100px;
		/*margin-top: 99px;*/
	}
	.gallery > li {
    float: left;
    margin-bottom: -2px !important;
    margin-right: 5px;
}
.fleft1 {
overflow: hidden;position: relative;top: -12px;
}
</style>	<div class="fleft1">
			
				<ul class="gallery clearfix">
				 	
					<li><a href="javascript: void(0)" onclick="javascript: RFP_Edit('<?php echo get_the_ID(); ?>')" title="Edit TaskList"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit.png" /></a></li>
					<li><a href="javascript: void(0)" onclick="javascript: RFP_Notes('<?php echo get_the_ID(); ?>')" title="Edit Notes"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/edit_icon.png" /></a></li>
					<li><a href="<?php echo get_post_meta($meta2, '_urladdress' , true);  ?>" rel="prettyPhoto" title="Video" ><img src="<?php echo RFP_PLUGIN_URL; ?>/images/video.png" /></a></li>
					<li><a href=# alt=Image Tooltip rel=tooltip content="<div id=con>Task Items:Task 1</div><div id=con>Category:SEO</div><div id=con>AddedBY:Admin</div><div id=con>AssignedTo:Rajeshkumar</div>"><img src="<?php echo RFP_PLUGIN_URL; ?>/images/star.png" /></a></li>
				</ul>
			</div>
<!--&nbsp;&nbsp;<a class="star" style="bottom: 35px !important;left: 90px;position: relative;" href="javascript: void(0)"></a>&nbsp;&nbsp;-->

</td>

<!-- End RFP Task List Action Section  -->
<td>
<input type="hidden" name="CheckValue" id="CheckValue" value="Check" /><input type="checkbox" name="checkbox[]" id="checkbox[]"  value="<?php echo get_the_ID(); ?>"/>
</td>
</tr>
 
<?php
endwhile;
wp_reset_query();


?>	
<!--End Loop and Query -->
</tbody>
</table>
	
<!-- End Task listing Table -->
</form>
</div>
	<?php

	
}
}
?>