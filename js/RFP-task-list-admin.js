jQuery( document ).ready( function( $ ) {

	$( '#rfptask-list' ).tablesorter();

	$( '#rfptask-list-completed th' ).click( function () {
		$( this ).parents( '#rfptask-list-completed' ).children( 'tbody' ).toggle();
		$( '#rfptask-list-completed #checkbox-col .icon' ).toggleClass( 'minus' ).toggleClass( 'plus' );
	} );

	$( "#rfp_task_assign" ).select2( {
		placeholder: rfp.SELECT_USER
	} );

	$( "#rfp_task-progress-slider" ).slider( {
		range:"min",
		value:$( "#rfp_task_progress" ).val(),
		min  :0,
		max  :100,
		step :5,
		slide:function ( event, ui ) {
			$( "#rfptask_progress" ).val( ui.value );
		}
	} );
	$( "#rfptask_progress" ).val( $( "#rfp_task-progress-slider" ).slider( "value" ) );

	var $rfp_task_dateformat = $( "#rfptask_format" ).val();
	$( "#RFP_date" ).datepicker( { dateFormat: $rfp_task_dateformat }  );
	
	var $RFP_task_dateformat = $( "#RFP_task_format" ).val();
	$( "#RFP_taskdate" ).datepicker( { dateFormat: $RFP_task_dateformat }  );

	$( "#rfp-resizable" ).resizable();

	$( '.rfptask-table tbody tr:visible:even' ).addClass( 'alternate' );

	/* Complete RFP-Tasks */
	$( '.rfptask-checkbox' ).click( function () {
		var action = '';
		var id = $( this ).attr( 'id' ).substr( 5 );
		var rfp_task_complete_nonce = $( "input[name=rfptask_complete_nonce]" ).val();
		if ( $( this ).hasClass( 'uncompleted' ) ) {
			action = 'completerfptask';
		} else if ( $( this ).hasClass( 'completed' ) ) {
			action = 'uncompleterfptask';
		}
		document.location.href = 'admin.php?page=rfp-task-list&action='+action+'&id='+id+'&_wpnonce='+rfp_task_complete_nonce;
	} );
	/* end Complete RFP-Tasks */

	/* Edit RFP-Tasks */
	$( '.edit-rfptask' ).live( 'click', function () {
		var rfptasktr = $( this ).closest( 'tr' );
		var id = $( rfptasktr ).attr( 'id' ).substr( 5 );
		document.location.href = 'admin.php?page=rfp-task-list&action=edit-rfptask&id='+id;
	} );
	/* end Edit RFP-Tasks */

	/* Delete Tables */
	$( '#delete-tables' ).live( 'click', function () {
		var confirmed = confirm( rfp.CONFIRMATION_DEL_TABLES_MSG );
		if ( confirmed == false ) return false;
	} );

	/* Delete All RFP-Tasks */
	$( '#delete-all-rfptasks' ).live( 'click', function () {
		var confirmed = confirm( rfp.CONFIRMATION_DELETE_ALL_MSG );
		if ( confirmed == false ) return false;
	} );

	/* Delete RFP-Tasks */
	$( '.delete-rfptask' ).live( 'click', function () {
		var confirmed = confirm( rfp.CONFIRMATION_MSG );
		if ( confirmed == false ) return false;
		var _item = this;
		var rfptasktr = $( _item ).closest( 'tr' );

		$.ajax({
         	type:'post',
        	url: ajaxurl,
	    	data: {
				action: 'cleverness_delete_rfptask',
				rfp_task_id: $( rfptasktr ).attr( 'id' ).substr( 5 ),
				_ajax_nonce: rfp.NONCE
			},
        	success: function( data ){
				if ( data == 1 ) {
					$( _item ).parent().html( '<p>'+rfp.SUCCESS_MSG+'</p>' ); // replace edit and delete buttons with message
					$( rfptr ).css( 'background', '#FFFFE0' ).delay( 2000 ).fadeOut( 400, function () { // change the table row background, fade, and remove row, re-stripe
						$( '#rfptask-list tbody tr').removeClass( 'alternate' );
						$( '#rfptask-list tbody tr:visible:even' ).addClass( 'alternate' );
					});
				} else if ( data == 0 ) {
					$( '#message' ).html( '<p>'+rfp.ERROR_MSG+'</p>' ).show().addClass( 'error below-h2' );
					$( rfptr ).css( 'background', '#FFEBE8' );
				} else if ( data == -1 ) {
					$( '#message' ).html( '<p>'+rfp.PERMISSION_MSG+'</p>' ).show().addClass( 'error below-h2' );
				}
      		},
      	    error: function( r ) {
				$( '#message' ).html( '<p>'+rfp.ERROR_MSG+'</p>' ).show().addClass( 'error below-h2' );
				$( rfptr ).css( 'background', '#FFEBE8' );
			}
    	} );
	} );
	/* end Delete RFP-Tasks */

} );