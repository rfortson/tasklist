<?php
/*
Plugin Name: Task List
Plugin URI: http://rfwebsiteprogramming.com/plugins/TaskList
Description: Wordpress Plugin for creating dynamic task list. Includes a dashboard widget, and shortcodes.
Version: The Plugin's Version Number, e.g.: 1.0
Author: Ramona
Author URI: http://rfwebsiteprogramming.com/plugins/TaskList
License: A "Slug" license name e.g. GPL2
*/

add_action('admin_menu','RFP_task_list');
include_once "includes/loader.inc.php";
include_once "includes/settings.inc.php";
include_once "includes/category.inc.php";
include_once "includes/library.inc.php";
include_once "includes/tasklist.inc.php";

add_action( 'init', 'register_my_taxonomies', 0 );
function register_my_taxonomies() {

	register_taxonomy(
		'rfpcat',
		array( 'post' ),
		array(
		'public' => true,
		'labels' => array(
		'name' => __( 'rfpcat' ),
		'singular_name' => __( 'rfpcat' )
			),
		)
	);
}

add_action('admin_head', 'register_my_option');


 function register_my_option(){

     $screen = get_current_screen(); //Initiate the $screen variable.
     add_filter('screen_layout_columns', 'display_my_option'); //Add our custom HTML to the screen options panel.
     $screen->add_option('my_option', ''); //Register our new screen options tab.
}

function display_my_option(){

if ($_REQUEST['submit']=='Apply') {

RFP_Lib::screenoptions();

}
global $wpdb;
$options = $wpdb->get_results("SELECT *	FROM wp_rfpscreenoptions WHERE id = 1");
foreach($options as $screen)  {
//print_r($screen);
$priority	=	 $screen->priority;
$duedate	=	 $screen->duedate;
$assigned	=	 $screen->assigned;
}

?>
<form name="screen" id="screen" action="" method="post" enctype="multipart/form-data">
<div class="control">
<input type="checkbox" name="Priority" id="Priority" <?php if (isset($priority)) if ($priority=='1') { ?> checked="checked" <?php } ?> value="1" />
<label for="check">Priority</label>
<input type="checkbox" id="Datedue" name="Datedue" <?php if (isset($duedate)) if ($duedate=='1') { ?> checked="checked" <?php } ?> value="1" />
<label for="check">Date Due</label>
<input type="checkbox" id="Assignedto" name="Assignedto" <?php if (isset($assigned)) if ($assigned=='1') { ?> checked="checked"<?php } ?> value="1" />

<label for="check">Show Assigned To</label>
<input type="submit" value="Apply" name="submit" class="button-secondary">
</div>
</form>
<?php
}
function save_my_option(){

     if(isset($_POST['my_option_submit']) AND $_POST['my_option_submit'] == 1){

     }

}

add_action('admin_init', 'save_my_option');





function RFP_dashboard_widget_function() {
	// Display whatever it is you want to show
	RFP_Loader::dashboard_widget();
} 

// Create the function use in the action hook

function RFP_dashboard_widgets() {
	wp_add_dashboard_widget('RFP_Task_dashboard_widget', 'Task List', 'RFP_dashboard_widget_function');	
} 

// Hook into the 'wp_dashboard_setup' action to register our other functions

add_action('wp_dashboard_setup', 'RFP_dashboard_widgets' ); // Hint: For Multisite Network Admin Dashboard use wp_network_dashboard_setup instead of wp_dashboard_setup.

function RFP_task_list() {

	if ( !defined( 'RFP_DB_VERSION' ) )     define( 'RFP_DB_VERSION', '3.21' ); // also update in Task_activation at the bottom of this file
	if ( !defined( 'RFP_PLUGIN_VERSION' ) ) define( 'RFP_PLUGIN_VERSION', '1.0' );
	if ( !defined( 'RFP_FILE' ) )           define( 'RFP_FILE', __FILE__ );
	if ( !defined( 'RFP_BASENAME' ) )       define( 'RFP_BASENAME', plugin_basename( __FILE__ ) );
	if ( !defined( 'RFP_PLUGIN_DIR' ) )     define( 'RFP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
	if ( !defined( 'RFP_PLUGIN_URL' ) )     define( 'RFP_PLUGIN_URL', plugins_url( '', __FILE__ ) );
	$options = get_option( 'rfplist_general' );
	add_menu_page('Task List', 'Task List', 'manage_options', 'my-menu', 'tasklist_admin',RFP_PLUGIN_URL."/images/quote_icon_1.png");
	
	add_submenu_page('my-menu', 'Add Task', 'Add Task', 'manage_options', 'Add-Task-List','add_task_list' );	
    if ($options['category']==1) {
    add_submenu_page('my-menu', 'Category-List', 'Category-List', 'manage_options', 'Category-List','task_category_list' );
	}
	add_submenu_page('my-menu', 'Settings', 'Settings', 'manage_options', 'Settings','RFP_settings' );
	

	}
function tasklist_admin() {


add_action( 'admin_print_styles', RFP_Loader::add_admin_css());
add_action( 'admin_print_styles', RFP_Loader::add_admin_js());
RFP_Tasklist::rfp_task_list();
	}
function add_task_list() {

add_action( 'admin_print_styles', RFP_Loader::add_admin_css());
add_action( 'admin_print_styles', RFP_Loader::add_admin_js());
RFP_Tasklist::new_tasklist();

}
function task_category_list() {

add_action( 'admin_print_styles', RFP_Loader::add_admin_css());
add_action( 'admin_print_styles', RFP_Loader::add_admin_js());
RFP_Category::category_page();

	
}

function RFP_settings() {

add_action( 'admin-settings', RFP_Settings::plugin_options_tabs());
$URI			=	$_SERVER['REQUEST_URI'];
$menus			=	explode('=',$URI);
$menu			=	$menus[count($menus)-1];

?>
<div class="wrap">
<div class="icon32"><img alt="" src="<?php echo RFP_PLUGIN_URL; ?>/images/quote_icon1.jpg" width="32" height="32"></div>
<h2 class="nav-tab-wrapper">
<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=general" class="<?php if($menu=='Settings' || $menu=='general') echo 'nav-tab nav-tab-active'; else  echo 'nav-tab';?>">Task List Settings</a>
<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=advanced" onclick="javascript: advanced()" class="<?php if($menu=='advanced') echo 'nav-tab nav-tab-active'; else echo 'nav-tab';?>">Advanced Settings</a>
<!--<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=userpermissions" class="<?php if($menu=='userpermissions') echo 'nav-tab nav-tab-active'; else echo  'nav-tab';?>">User Permissions</a>-->
<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=importexport" class="<?php if($menu=='importexport') echo 'nav-tab nav-tab-active';else echo 'nav-tab'?>">Import/Export</a>
<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=help" class="<?php if($menu=='help') echo 'nav-tab nav-tab-active';else echo 'nav-tab'?>">Task Help</a>
<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=shortcode" class="<?php if($menu=='shortcode') echo 'nav-tab nav-tab-active';else echo 'nav-tab'?>">Shortcodes</a>
<a href="<?php echo admin_url(); ?>admin.php?page=Settings&tab=faqs" class="<?php if($menu=='faqs') echo 'nav-tab nav-tab-active';else echo 'nav-tab'?>">FAQs</a>
</h2>
<?php
if ($menu=='Settings' || $menu=='general') {
	RFP_Settings::task_list_settings();
} elseif ($menu=='advanced') {
		RFP_Settings::task_list_advanced();
} elseif ($menu=='userpermissions') {
		RFP_Settings::task_list_userpermissions();
} elseif ($menu=='importexport') {
		RFP_Settings::task_list_importexport();
} elseif ($menu=='export') {

	RFP_Settings::task_list_export();
} elseif($menu=='help')  {
	RFP_Settings::task_list_help();
} elseif ($menu=='shortcode') {
	RFP_Settings::task_list_shortcodes();
} elseif ($menu=='faqs') {
	RFP_Settings::task_list_faqs();
}
?>

	</div>
<?php

}
/**
 * Install plugin on plugin activation
 */
function RFT_Task_activation() {
	
		RFP_Lib::install_plugin();
		RFP_Lib::set_options();
		RFP_Lib::register_my_taxonomies();
		RFP_Lib::populate_taxonomies();
}

register_activation_hook( __FILE__, 'RFT_Task_activation' );	


/**********
***********
***********

Registering the Shortcode for the RFP Task List


***********
***********
***********
**********/

function recent_checklist_function($atts) {
extract(shortcode_atts(array(
	    'posts' => 10,
   ), $atts));
   
global $post;
$user_id = get_current_user_id();	  
$args = array(
			 'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'order'    => 'DESC',
			   'author'   => $user_id,
			   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 )
				  ); 			  
			  
			  
$flag	=	1;


if ($user_id == 0) {
    echo '<strong>You must be logged in to view</strong>';
} else {
    //echo 'You are logged in as user '.$user_id;
?>


<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
					<th scope="row"><label></label></th>
					<th scope="row"><label>Item</label></th>
					<th scope="row"><label>AssignedTo</label></th>
					<th scope="row"><label>AddedBy</label></th>
					<th scope="row"><label>Due Date</label></th>
					<th scope="row"><label>Priority</label></th>
					
			</tr>
<?php

$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();
 $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 $meta2= get_the_ID(); 

?> 
<div id="success">
		
			<tr>
				<td>
				
				 
				 <input type="checkbox" name="post_id" id="<?php echo $meta2; ?>"  class="user_vote">
			
				</td>
				
				<td><?php  RFP_TaskList::the_title1(); ?></td>
				<td><?php echo get_post_meta($meta2, '_assign' , true);  ?></td>
				<td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td>
				<td><?php echo get_post_meta($meta2, '_Date' , true);  ?></td>
				<td><?php echo $priority	=	get_post_meta($meta2, '_priority' , true); ?></td>
			</tr>
		
	</div>

<?php
$flag++;
endwhile;
wp_reset_query();
?>
</tbody>
	</table>
<?php
	}
}
function recent_displaychecklist_function($atts) {

extract(shortcode_atts(array(
		'title' => '',
		'taskname' => '',
		'priority' => '',
		'category' => '',
		'timeframe' => '',
		'assignedto' => '',
		'status' => '',
		'duedate' => '',
		'addedby' => '',
	    'posts' => 10,
   ), $atts));
   
global $post;
$user_id = get_current_user_id();	  
$args = array(
			 'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'order'    => 'DESC',
			   'author'   => $user_id,
			   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 )
				  ); 			  
			  
			  
$flag	=	1;


if ($user_id == 0) {
    echo '<strong>You must be logged in to view</strong>';
} else {
    //echo 'You are logged in as user '.$user_id;
?>


<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
			<th scope="row"><label><strong><?php echo $title; ?></strong></label></th>
		</tr>
		<tr>
					<th scope="row"><label></label></th>
					<th scope="row"><label>Category</label></th>
					<?php if ($taskname=='hide') {} else { ?><th scope="row"><label>Item</label></th><?php }?>
					<?php if ($assignedto=='hide') {} else { ?><th scope="row"><label>AssignedTo</label></th><?php } ?>
					<?php if ($addedby=='hide') {} else { ?><th scope="row"><label>AddedBy</label></th><?php } ?>
					<?php if ($duedate=='hide') {} else { ?><th scope="row"><label>Due Date</label></th><?php } ?>
					<?php if ($status=='hide') {} else { ?><th scope="row"><label>Status</label></th><?php } ?>
					<?php if ($priority=='show') { ?><th scope="row"><label>Priority</label></th><?php } ?>
					<?php if ($timeframe=='show') { ?><th scope="row"><label>Time Frame</label></th><?php } ?>
					
			</tr>
<?php

$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();
 $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 $meta2= get_the_ID(); 

?> 
<div id="success">
		
			<tr>
				<td>
				
				 
				 <input type="checkbox" name="post_id" id="<?php echo $meta2; ?>"  class="user_vote">
			
				</td>
				
				<td><?php  if ($term_list) { echo $term_list[0]->name; } ?></td>
				<?php if ($taskname=='hide') {} else { ?><td><?php  RFP_TaskList::the_title1(); ?></td><?php } ?>
				<?php if ($assignedto=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_assign' , true);  ?></td><?php } ?>
				<?php if ($addedto=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td><?php } ?>
				<?php if ($duedate=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_Date' , true);  ?></td><?php } ?>
				<?php if ($status=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></td><?php } ?>
				<?php if ($priority=='show') { ?><td><?php echo get_post_meta($meta2, '_priority' , true);  ?></td><?php } ?>
				<?php if ($timeframe=='show') { ?><td><?php echo get_post_meta($meta2, '_TimeFrame' , true);  ?></td><?php } ?>
			</tr>
		
	</div>

<?php
$flag++;
endwhile;
wp_reset_query();
?>
</tbody>
	</table>
<?php
	}

}
function recent_posts_function($atts){
   extract(shortcode_atts(array(
      'posts' => 10,
   ), $atts));
   
global $post;
$args = array(
			 'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred','Completed'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'order'    => 'DESC',
			   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 )
				  ); 
$flag	=	1;
?>
<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
					<th scope="row"><label>Category</label></th>
					<th scope="row"><label>Item</label></th>
					<th scope="row"><label>AssignedTo</label></th>
					<th scope="row"><label>AddedBy</label></th>
					<th scope="row"><label>Due Date</label></th>
					<th scope="row"><label>Status</label></th>
					
			</tr>
<?php
$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();
 $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 $meta2= get_the_ID(); 
?> 
		
			<tr>
				<td><?php  if ($term_list) { echo $term_list[0]->name; } ?></td>
				
				<td><?php  RFP_TaskList::the_title1(); ?></td>
				<td><?php echo get_post_meta($meta2, '_assign' , true);  ?></td>
				<td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td>
				<td><?php echo get_post_meta($meta2, '_Date' , true);  ?></td>
				<td><?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></td>
			</tr>
		


<?php
$flag++;
endwhile;
wp_reset_query();
?>
</tbody>
	</table>
<?php
}

function recent_posts_display_function($atts) {

   extract(shortcode_atts(array(
   		'title' => '',
		'taskname' => '',
		'priority' => '',
		'category' => '',
		'timeframe' => '',
		'assignedto' => '',
		'status' => '',
		'duedate' => '',
		'addedby' => '',
		'list_type' =>'',
      	'posts' => 10,
   ), $atts));
global $wpdb;
//$sql_sel	=	$wpdb->get_results("SELECT * FROM wp_term_relationships WHERE term_taxonomy_id=88");  
//echo $category;
global $post;
$args = array(
			 'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred','Completed'))),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'order'    => 'DESC',
			   //'cat' => $category,
			   //'tax_query'=> array('terms' => array('term_taxonomy_id' => 88)),
			   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 )
				  ); 
$flag	=	1;
if ($list_type=='ol') {
?>
<div class="entry-content">
			<p><strong><strong></strong></strong></p>
			<ol></ol>
			<?php
$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();
 $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 $meta2= get_the_ID(); 
?> 

			<h4>
			<strong><strong><?php  if ($term_list) { echo $term_list[0]->name; } ?></strong></strong>
			</h4>
			<ol>
			<li><strong><strong>Item - <?php  RFP_TaskList::the_title1(); ?> - Assigned To: <?php echo get_post_meta($meta2, '_assign' , true);  ?> - Added By: <?php echo get_post_meta($meta2, '_addedto' , true);  ?> - Due Date: <?php echo get_post_meta($meta2, '_Date' , true);  ?> - Status: <?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></strong></strong></li>
			</ol>
	<?php

endwhile;
wp_reset_query();
?>		
			<strong><br>
			</strong><p></p>
</div>

<?php
} else {
?>
<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
			<th scope="row"><label><strong><?php echo $title; ?></strong></label></th>
		</tr>
		
		<tr>
					<th scope="row"><label>Category</label></th>
					<?php if ($taskname=='hide') {} else { ?><th scope="row"><label>Item</label></th><?php }?>
					<?php if ($assignedto=='hide') {} else { ?><th scope="row"><label>AssignedTo</label></th><?php } ?>
					<?php if ($addedby=='hide') {} else { ?><th scope="row"><label>AddedBy</label></th><?php } ?>
					<?php if ($duedate=='hide') {} else { ?><th scope="row"><label>Due Date</label></th><?php } ?>
					<?php if ($status=='hide') {} else { ?><th scope="row"><label>Status</label></th><?php } ?>
					<?php if ($priority=='show') { ?><th scope="row"><label>Priority</label></th><?php } ?>
					<?php if ($timeframe=='show') { ?><th scope="row"><label>Time Frame</label></th><?php } ?>
					
			</tr>
<?php
$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();
 $term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
 $meta2= get_the_ID(); 
?> 
		
			<tr>
				<td><?php  if ($term_list) { echo $term_list[0]->name; } ?></td>
				<?php if ($taskname=='hide') {} else { ?><td><?php  RFP_TaskList::the_title1(); ?></td><?php } ?>
				<?php if ($assignedto=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_assign' , true);  ?></td><?php } ?>
				<?php if ($addedto=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_addedto' , true);  ?></td><?php } ?>
				<?php if ($duedate=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_Date' , true);  ?></td><?php } ?>
				<?php if ($status=='hide') {} else { ?><td><?php echo get_post_meta($meta2, '_rfpstatus' , true);  ?></td><?php } ?>
				<?php if ($priority=='show') { ?><td><?php echo get_post_meta($meta2, '_priority' , true);  ?></td><?php } ?>
				<?php if ($timeframe=='show') { ?><td><?php echo get_post_meta($meta2, '_TimeFrame' , true);  ?></td><?php } ?>
			</tr>
		


<?php
$flag++;
endwhile;
wp_reset_query();
?>
</tbody>
	</table>
<?php
	}
}	

function rfptasklist() {
	add_shortcode('TaskList', 'recent_posts_function');
}
function rfptasklistdisplay() {
	add_shortcode('TaskListDisplay', 'recent_posts_display_function');
}
function rfpchecklist() {
add_shortcode('CheckList', 'recent_checklist_function');
}


function rfpdisplaylist() {
add_shortcode('CheckListDisplay', 'recent_displaychecklist_function');
}

add_action( 'init', 'rfptasklist');
add_action( 'init', 'rfptasklistdisplay');
add_action( 'init', 'rfpdisplaylist');
add_action( 'init', 'rfpchecklist');

/**************************
Front end check box script
*************************/

 add_action( 'init', 'my_script_enqueuer' );

function my_script_enqueuer() {
	$p_url	=	 plugins_url( '', __FILE__ );
   wp_register_script( "my_voter_script", $p_url.'/js/ajaxupdate.js', array('jquery') );
   wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        

   wp_enqueue_script( 'jquery' );
   wp_enqueue_script( 'my_voter_script' );

}


add_action("wp_ajax_my_user_vote", "my_user_vote");
add_action("wp_ajax_nopriv_my_user_vote", "my_must_login");

function my_user_vote() {

   $vote = update_post_meta($_REQUEST["post_id"], "_rfpstatus", 'completed');

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	echo  '';
   }
   else {
      header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}

function my_must_login() {
   echo "You must log in to view";
   die();
}
/******************
//Recurring Function
*/
function wp_schedule_events( $timestamp, $recurrence, $hook, $args = array()) {
	$crons = _get_cron_array();
	$schedules = wp_get_scheduless();

	if ( !isset( $schedules[$recurrence] ) )
		return false;

	$event = (object) array( 'hook' => $hook, 'timestamp' => $timestamp, 'schedule' => $recurrence, 'args' => $args, 'interval' => $schedules[$recurrence]['interval'] );
	$event = apply_filters('schedule_event', $event);

	// A plugin disallowed this event
	if ( ! $event )
		return false;

	$key = md5(serialize($event->args));

	$crons[$event->timestamp][$event->hook][$key] = array( 'schedule' => $event->schedule, 'args' => $event->args, 'interval' => $event->interval );
	uksort( $crons, "strnatcasecmp" );
	_set_cron_array( $crons );
}

function wp_get_scheduless() {
	$num = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
	$schedules = array(
		'hourly'     => array( 'interval' => HOUR_IN_SECONDS,      'display' => __( 'Once Hourly' ) ),
		'twicedaily' => array( 'interval' => 12 * HOUR_IN_SECONDS, 'display' => __( 'Twice Daily' ) ),
		'daily'      => array( 'interval' => DAY_IN_SECONDS,       'display' => __( 'Once Daily' ) ),
		'weekly'     => array( 'interval' => 60*60*24*7,          'display' => __( 'Once weekly' ) ),
		'monthly'	 => array( 'interval' => 60*60*24*$num,    	  'display' => __( 'Once Monthly' ) ),
		'twicemonth'	 => array( 'interval' => 60*60*24*15,    	  'display' => __( 'Twice Monthly' ) ),
	);
	return array_merge( apply_filters( 'cron_schedules', array() ), $schedules );
}
$advanced  = get_option('rfplist_advanced');
$recurring	=	$advanced['RecurringTask'];
if ( ! wp_next_scheduled('my_task_hooks') ) {
    wp_schedule_events( time(), $recurring, 'my_task_hooks' ); // hourly, daily and twicedaily
}
 
add_action( 'my_task_hooks', 'rfpcron' );

function rfpcron()
{

    global $post;
	$date1	=	date('m/d/Y');
    $args = array(
			 'posts_per_page'  => 10,
			   'numberposts'     => 10,
			   'meta_query' => array(array('relation'=>'AND'),array('key' => '_rfpstatus', 'value' => array('Not Started','In Progress','Irrelevant','Deferred')),array('key' => '_Date', 'value' => $date1, 'compare' => '<')),
			   'post_type' => 'rfptask',
			   'taxonomy' => 'rfpcat',
			   'orderby'  => 'ID',
			   'order'    => 'DESC',
			   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 )
				); 			  

$flag	=	1;

$rfpquery = new WP_Query($args);
while ($rfpquery->have_posts()) : $rfpquery->the_post();
$term_list = wp_get_post_terms($post->ID, 'rfpcat', array("fields" => "all"));
$meta2= get_the_ID(); 

$userdata=get_userdata($post->post_author);
$email=$userdata->user_email;
		add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html";' ) );
		//print_r($_REQUEST);exit;
	
	
					$headers = 'From: '.$advanced['email_from'].' <'.get_bloginfo( 'admin_email' ).'>'."\r\n\\";
					$subject = 'Due Date';
					$email_message=$post->post_title.' not completed yet';
					wp_mail( $email, $subject, $email_message, $headers );
				
		

$flag++;
endwhile;
wp_reset_query();
?>
</tbody>
	</table>
    <?php
}

?>

